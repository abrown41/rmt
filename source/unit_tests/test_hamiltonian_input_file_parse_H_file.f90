MODULE test_hamiltonian_input_file_parse_H_file
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE testing_utilities, ONLY: compare, &
                                 data_file, &
                                 format_context, &
                                 int_to_char, &
                                 range, &
                                 subarray, &
                                 subarray_integer_int32, &
                                 subarray_real_real64

    USE precisn, ONLY: wp
    USE hamiltonian_input_file, ONLY: H_file, L_block, parse_H_file

    IMPLICIT NONE

    INTERFACE check
        MODULE PROCEDURE check_H_file
    END INTERFACE

    TYPE expected_H_file
        INTEGER(INT32) :: nelc
        INTEGER(INT32) :: nz
        INTEGER(INT32) :: lrang2
        INTEGER(INT32) :: lamax
        INTEGER(INT32) :: ntarg
        REAL(REAL64) :: rmatr
        REAL(REAL64) :: bbloch

        REAL(REAL64), DIMENSION(:), ALLOCATABLE :: etarg
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE :: ltarg
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE :: starg

        REAL(REAL64), DIMENSION(:, :), ALLOCATABLE :: cfbut

        TYPE(expected_L_block), DIMENSION(:), ALLOCATABLE :: L_blocks
    END TYPE

    TYPE expected_L_block
        INTEGER(INT32) :: lrgl
        INTEGER(INT32) :: nspn
        INTEGER(INT32) :: npty
        INTEGER(INT32) :: nchan
        INTEGER(INT32) :: nstat

        INTEGER(INT32), DIMENSION(:), ALLOCATABLE :: nconat
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE :: l2p
        TYPE(subarray_real_real64) :: cf
        TYPE(subarray_real_real64) :: eig
        TYPE(subarray_real_real64) :: wmat
    END TYPE

    PRIVATE
    PUBLIC :: collect_hamiltonian_input_file_parse_H_file
CONTAINS
    SUBROUTINE collect_hamiltonian_input_file_parse_H_file(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [new_unittest("atomic::small::Ne_4cores", test_atomic_small_Ne_4cores), &
                     new_unittest("atomic::small::ar+", test_atomic_small_arx), &
                     new_unittest("atomic::small::argon", test_atomic_small_argon), &
                     new_unittest("atomic::small::helium", test_atomic_small_helium), &
                     new_unittest("atomic::small::iodine", test_atomic_small_iodine), &
                     new_unittest("RMatrixI::example", test_rmatrixi_example)]
    END SUBROUTINE

    SUBROUTINE check_L_block(context, index, ntarg, lamax, actual, expected)
        CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
        INTEGER, INTENT(IN) :: index, ntarg, lamax
        TYPE(L_block), INTENT(IN) :: actual
        TYPE(expected_L_block), INTENT(IN) :: expected

        CHARACTER(len=:), ALLOCATABLE :: prefix
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape

        prefix = "L_blocks("//int_to_char(index)//")%"

        IF (actual%lrgl /= expected%lrgl) THEN
            CALL format_context(context, prefix//"lrgl", actual%lrgl, expected%lrgl)
        END IF

        IF (actual%nspn /= expected%nspn) THEN
            CALL format_context(context, prefix//"nspn", actual%nspn, expected%nspn)
        END IF

        IF (actual%npty /= expected%npty) THEN
            CALL format_context(context, prefix//"npty", actual%npty, expected%npty)
        END IF

        IF (actual%nchan /= expected%nchan) THEN
            CALL format_context(context, prefix//"nchan", actual%nchan, expected%nchan)
        END IF

        IF (actual%nstat /= expected%nstat) THEN
            CALL format_context(context, prefix//"nstat", actual%nstat, expected%nstat)
        END IF

        IF (.NOT. compare(actual%nconat, expected%nconat)) THEN
            CALL format_context(context, prefix//"nconat", actual%nconat, expected%nconat)
        END IF

        IF (.NOT. compare(actual%l2p, expected%l2p)) THEN
            CALL format_context(context, prefix//"l2p", actual%l2p, expected%l2p)
        END IF

        IF (.NOT. compare(actual%cf, expected%cf)) THEN
            CALL format_context(context, prefix//"cf", actual%cf, expected%cf)
        END IF

        IF (.NOT. compare(actual%eig, expected%eig)) THEN
            CALL format_context(context, prefix//"eig", actual%eig, expected%eig)
        END IF

        IF (.NOT. compare(actual%wmat, expected%wmat)) THEN
            CALL format_context(context, prefix//"wmat", actual%wmat, expected%wmat)
        END IF

        expected_shape = [ntarg]
        IF (.NOT. compare(SHAPE(actual%nconat), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%nconat)", SHAPE(actual%nconat), expected_shape)
        END IF

        expected_shape = [actual%nchan]
        IF (.NOT. compare(SHAPE(actual%l2p), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%l2p)", SHAPE(actual%l2p), expected_shape)
        END IF

        expected_shape = [actual%nchan, actual%nchan, lamax]
        IF (.NOT. compare(SHAPE(actual%cf), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%cf)", SHAPE(actual%cf), expected_shape)
        END IF

        expected_shape = [actual%nstat]
        IF (.NOT. compare(SHAPE(actual%eig), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%eig)", SHAPE(actual%eig), expected_shape)
        END IF

        expected_shape = [actual%nchan, actual%nstat]
        IF (.NOT. compare(SHAPE(actual%wmat), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%wmat)", SHAPE(actual%wmat), expected_shape)
        END IF
    END SUBROUTINE

    SUBROUTINE check_H_file(error, actual, expected)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(H_file), INTENT(in) :: actual
        TYPE(expected_H_file), INTENT(in) :: expected

        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape
        INTEGER :: i

        context = ""

        IF (actual%nelc /= expected%nelc) THEN
            CALL format_context(context, "nelc", actual%nelc, expected%nelc)
        END IF

        IF (actual%nz /= expected%nz) THEN
            CALL format_context(context, "nz", actual%nz, expected%nz)
        END IF

        IF (actual%lrang2 /= expected%lrang2) THEN
            CALL format_context(context, "lrang2", actual%lrang2, expected%lrang2)
        END IF

        IF (actual%lamax /= expected%lamax) THEN
            CALL format_context(context, "lamax", actual%lamax, expected%lamax)
        END IF

        IF (actual%ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual%ntarg, expected%ntarg)
        END IF

        IF (.NOT. compare(actual%rmatr, expected%rmatr)) THEN
            CALL format_context(context, "rmatr", actual%rmatr, expected%rmatr)
        END IF

        IF (.NOT. compare(actual%bbloch, expected%bbloch)) THEN
            CALL format_context(context, "bbloch", actual%bbloch, expected%bbloch)
        END IF

        IF (.NOT. compare(actual%etarg, expected%etarg)) THEN
            CALL format_context(context, "etarg", actual%etarg, expected%etarg)
        END IF

        IF (.NOT. compare(actual%ltarg, expected%ltarg)) THEN
            CALL format_context(context, "ltarg", actual%ltarg, expected%ltarg)
        END IF

        IF (.NOT. compare(actual%starg, expected%starg)) THEN
            CALL format_context(context, "starg", actual%starg, expected%starg)
        END IF

        IF (.NOT. compare(actual%cfbut, expected%cfbut)) THEN
            CALL format_context(context, "cfbut", actual%cfbut, expected%cfbut)
        END IF

        ! Check that array shapes are _internally_ consistent.
        ! The file stores the shapes of the arrays that it uses so we want to make sure that we're reading that correctly.
        ! For example the `cfbut` array should have dimensions (3, lrang2).
        ! We compare the shape to the actual values rather than the expected values for two reasons:
        !   * If we compare with the expected values then we could end up in a situation where the tests parse even though the file has been passed wrong.
        !   * When first writing a test it can be confusing if the expected values have been filled with default values.

        expected_shape = [actual%ntarg]
        IF (.NOT. compare(SHAPE(actual%etarg), expected_shape)) THEN
            CALL format_context(context, "shape(etarg)", SHAPE(actual%etarg), expected_shape)
        END IF

        expected_shape = [actual%ntarg]
        IF (.NOT. compare(SHAPE(actual%ltarg), expected_shape)) THEN
            CALL format_context(context, "shape(ltarg)", SHAPE(actual%ltarg), expected_shape)
        END IF

        expected_shape = [actual%ntarg]
        IF (.NOT. compare(SHAPE(actual%starg), expected_shape)) THEN
            CALL format_context(context, "shape(starg)", SHAPE(actual%starg), expected_shape)
        END IF

        expected_shape = [3, actual%lrang2]
        IF (.NOT. compare(SHAPE(actual%cfbut), expected_shape)) THEN
            CALL format_context(context, "shape(cfbut)", SHAPE(actual%cfbut), expected_shape)
        END IF

        ! Check L Blocks.

        IF (SIZE(actual%L_blocks) /= SIZE(expected%L_blocks)) THEN
            CALL format_context(context, "size(L_blocks)", SIZE(actual%L_blocks), SIZE(expected%L_blocks))
        ELSE
            DO i = 1, SIZE(expected%L_blocks)
                CALL check_L_block(context, i, actual%ntarg, actual%lamax, actual%L_blocks(i), expected%L_blocks(i))
            END DO
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "H file parsed incorrectly!", context)
        END IF
    END SUBROUTINE

    SUBROUTINE test_H_file(error, path, expected)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        CHARACTER(len=*), INTENT(in) :: path
        TYPE(expected_H_file), INTENT(in) :: expected

        TYPE(H_file) :: file
        CHARACTER(len=:), ALLOCATABLE :: H_file_path

        H_file_path = data_file(path)
        file = parse_H_file(H_file_path)

        CALL check(error, file, expected)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_Ne_4cores(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected

        expected = expected_H_file( &
            nelc=9_INT32, &
            nz=10_INT32, &
            lrang2=4_INT32, &
            lamax=4_INT32, &
            ntarg=2_INT32, &
            rmatr=20.0_REAL64, &
            bbloch=0.0_REAL64, &
            etarg=[-1.279277e+02_REAL64, -1.269274e+02_REAL64], &
            ltarg=[1_INT32, 0_INT32], &
            starg=[2_INT32, 2_INT32], &
            cfbut=RESHAPE( &
                [ &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00 &
                ], &
                [3, 4] &
            ), &
            L_blocks=[ &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=2_INT32, &
                    nstat=134_INT32, &
                    nconat=[1_INT32, 1_INT32], &
                    l2p=[1_INT32, 0_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(3, 4)], &
                        [2, 2, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -1.170405e+02_REAL64, &
                            -1.165479e+02_REAL64, &
                            -1.161594e+02_REAL64, &
                            -1.157827e+02_REAL64, &
                            -1.152606e+02_REAL64, &
                            -1.139737e+02_REAL64, &
                            -1.124227e+02_REAL64, &
                            -1.114233e+02_REAL64, &
                            -1.096727e+02_REAL64 &
                        ], &
                        [range(103, 111)], &
                        [134] &
                    ), &
                    wmat=subarray( &
                        [ &
                            3.269528e-01_REAL64, &
                            3.042853e-04_REAL64, &
                            -5.158848e-02_REAL64, &
                            1.477980e-03_REAL64, &
                            2.715547e-01_REAL64, &
                            6.753705e-02_REAL64, &
                            1.698242e-01_REAL64, &
                            -1.782714e-01_REAL64 &
                        ], &
                        [range(1, 2), range(9, 12)], &
                        [2, 134] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=141_INT32, &
                    nconat=[1_INT32, 0_INT32], &
                    l2p=[1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            4.276485e-01_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(1, 2)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -1.204356e+02_REAL64, &
                            -1.203921e+02_REAL64, &
                            -1.203814e+02_REAL64, &
                            -1.203042e+02_REAL64, &
                            -1.202818e+02_REAL64, &
                            -1.201958e+02_REAL64, &
                            -1.201527e+02_REAL64, &
                            -1.201452e+02_REAL64, &
                            -1.201132e+02_REAL64 &
                        ], &
                        [range(79, 87)], &
                        [141] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -4.460979e-02_REAL64, &
                            -5.805166e-03_REAL64, &
                            -1.618237e-02_REAL64, &
                            -1.438857e-01_REAL64 &
                        ], &
                        [range(1, 1), range(68, 71)], &
                        [1, 141] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=3_INT32, &
                    nstat=233_INT32, &
                    nconat=[2_INT32, 1_INT32], &
                    l2p=[0_INT32, 2_INT32, 1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -4.658991e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -6.588808e-01_REAL64, &
                            -6.047864e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -4.276485e-01_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(1, 2), range(1, 2)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -1.180768e+02_REAL64, &
                            -1.179093e+02_REAL64, &
                            -1.178687e+02_REAL64, &
                            -1.178322e+02_REAL64, &
                            -1.178013e+02_REAL64, &
                            -1.177383e+02_REAL64, &
                            -1.175199e+02_REAL64, &
                            -1.174640e+02_REAL64, &
                            -1.174377e+02_REAL64 &
                        ], &
                        [range(176, 184)], &
                        [233] &
                    ), &
                    wmat=subarray( &
                        [ &
                            8.707377e-02_REAL64, &
                            2.979145e-01_REAL64, &
                            2.353909e-02_REAL64, &
                            -3.843663e-01_REAL64, &
                            -1.704863e-02_REAL64, &
                            -3.535556e-01_REAL64, &
                            -6.614243e-03_REAL64, &
                            -2.442725e-02_REAL64 &
                        ], &
                        [range(2, 3), range(20, 23)], &
                        [3, 233] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=3_INT32, &
                    nstat=254_INT32, &
                    nconat=[2_INT32, 1_INT32], &
                    l2p=[1_INT32, 3_INT32, 2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -5.103669e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -6.250693e-01_REAL64, &
                            -6.285124e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -3.421188e-01_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(1, 2), range(1, 2)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -1.184462e+02_REAL64, &
                            -1.182547e+02_REAL64, &
                            -1.181934e+02_REAL64, &
                            -1.181679e+02_REAL64, &
                            -1.181419e+02_REAL64, &
                            -1.180957e+02_REAL64, &
                            -1.179325e+02_REAL64, &
                            -1.178447e+02_REAL64, &
                            -1.176817e+02_REAL64 &
                        ], &
                        [range(188, 196)], &
                        [254] &
                    ), &
                    wmat=subarray( &
                        [ &
                            5.304610e-04_REAL64, &
                            2.033062e-03_REAL64, &
                            5.802956e-04_REAL64, &
                            -1.183335e-02_REAL64, &
                            -3.115233e-04_REAL64, &
                            2.845149e-02_REAL64, &
                            -7.225638e-04_REAL64, &
                            7.988044e-01_REAL64 &
                        ], &
                        [range(2, 3), range(177, 180)], &
                        [3, 254] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=1_INT32, &
                    nstat=165_INT32, &
                    nconat=[1_INT32, 0_INT32], &
                    l2p=[2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(3, 4)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -1.246643e+02_REAL64, &
                            -1.244170e+02_REAL64, &
                            -1.243865e+02_REAL64, &
                            -1.243226e+02_REAL64, &
                            -1.242254e+02_REAL64, &
                            -1.241014e+02_REAL64, &
                            -1.240241e+02_REAL64, &
                            -1.239966e+02_REAL64, &
                            -1.239505e+02_REAL64 &
                        ], &
                        [range(20, 28)], &
                        [165] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -1.273079e-03_REAL64, &
                            8.130593e-03_REAL64, &
                            3.292430e-03_REAL64, &
                            1.936230e-03_REAL64 &
                        ], &
                        [range(1, 1), range(78, 81)], &
                        [1, 165] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/Ne_H", expected)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_arx(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected

        expected = expected_H_file( &
            nelc=16_INT32, &
            nz=18_INT32, &
            lrang2=6_INT32, &
            lamax=4_INT32, &
            ntarg=1_INT32, &
            rmatr=15.0_REAL64, &
            bbloch=0.0_REAL64, &
            etarg=[-5.252291e+02_REAL64], &
            ltarg=[2_INT32], &
            starg=[1_INT32], &
            cfbut=RESHAPE( &
                [ &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00 &
                ], &
                [3, 6] &
            ), &
            L_blocks=[ &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=2_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=49_INT32, &
                    nconat=[1_INT32], &
                    l2p=[2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -2.022752e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(1, 2)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.093365e+02_REAL64, &
                            -5.076697e+02_REAL64, &
                            -5.045609e+02_REAL64, &
                            -5.007559e+02_REAL64, &
                            -5.002554e+02_REAL64, &
                            -4.945238e+02_REAL64, &
                            -4.863600e+02_REAL64, &
                            -4.766012e+02_REAL64, &
                            -4.744484e+02_REAL64 &
                        ], &
                        [range(28, 36)], &
                        [49] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -8.154209e+00_REAL64, &
                            -4.313977e-03_REAL64, &
                            1.243162e-03_REAL64, &
                            -7.172151e-04_REAL64 &
                        ], &
                        [range(1, 1), range(44, 47)], &
                        [1, 49] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=2_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=48_INT32, &
                    nconat=[1_INT32], &
                    l2p=[2_INT32], &
                    cf=subarray( &
                        [ &
                            -1.011376e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(2, 3)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -4.743989e+02_REAL64, &
                            -4.562891e+02_REAL64, &
                            -4.272505e+02_REAL64, &
                            -3.829377e+02_REAL64, &
                            -3.776718e+02_REAL64, &
                            -2.864064e+02_REAL64, &
                            -1.014520e+02_REAL64, &
                            3.224909e+02_REAL64, &
                            8.188017e+02_REAL64 &
                        ], &
                        [range(35, 43)], &
                        [48] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -3.695841e-01_REAL64, &
                            -3.692632e-01_REAL64, &
                            -3.690284e-01_REAL64, &
                            -3.689494e-01_REAL64 &
                        ], &
                        [range(1, 1), range(17, 20)], &
                        [1, 48] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=2_INT32, &
                    npty=1_INT32, &
                    nchan=2_INT32, &
                    nstat=95_INT32, &
                    nconat=[2_INT32], &
                    l2p=[1_INT32, 3_INT32], &
                    cf=subarray( &
                        [ &
                            -1.415927e+00_REAL64, &
                            -4.954711e-01_REAL64, &
                            -4.954711e-01_REAL64, &
                            -1.618202e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(2, 3)], &
                        [2, 2, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.018736e+02_REAL64, &
                            -5.007141e+02_REAL64, &
                            -5.006511e+02_REAL64, &
                            -4.993159e+02_REAL64, &
                            -4.967307e+02_REAL64, &
                            -4.929327e+02_REAL64, &
                            -4.896630e+02_REAL64, &
                            -4.837365e+02_REAL64, &
                            -4.795783e+02_REAL64 &
                        ], &
                        [range(58, 66)], &
                        [95] &
                    ), &
                    wmat=subarray( &
                        [ &
                            1.619926e+00_REAL64, &
                            5.943869e-03_REAL64, &
                            5.938898e-03_REAL64, &
                            -1.619187e+00_REAL64, &
                            -2.329781e-02_REAL64, &
                            -8.694838e-05_REAL64, &
                            5.580029e-05_REAL64, &
                            -1.308228e-02_REAL64 &
                        ], &
                        [range(1, 2), range(67, 70)], &
                        [2, 95] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=2_INT32, &
                    npty=0_INT32, &
                    nchan=3_INT32, &
                    nstat=141_INT32, &
                    nconat=[3_INT32], &
                    l2p=[0_INT32, 2_INT32, 4_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(2, 3), range(3, 4)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -4.930594e+02_REAL64, &
                            -4.923013e+02_REAL64, &
                            -4.862534e+02_REAL64, &
                            -4.845841e+02_REAL64, &
                            -4.825033e+02_REAL64, &
                            -4.766108e+02_REAL64, &
                            -4.766005e+02_REAL64, &
                            -4.765642e+02_REAL64, &
                            -4.743294e+02_REAL64 &
                        ], &
                        [range(93, 101)], &
                        [141] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -3.138637e-04_REAL64, &
                            5.830483e-03_REAL64, &
                            -4.304915e-02_REAL64, &
                            -3.776675e-01_REAL64, &
                            -4.385891e-01_REAL64, &
                            4.703239e-02_REAL64, &
                            -7.323251e-04_REAL64, &
                            1.197491e-02_REAL64 &
                        ], &
                        [range(1, 2), range(73, 76)], &
                        [3, 141] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=2_INT32, &
                    npty=1_INT32, &
                    nchan=2_INT32, &
                    nstat=94_INT32, &
                    nconat=[2_INT32], &
                    l2p=[1_INT32, 3_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(3, 4)], &
                        [2, 2, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.243575e+02_REAL64, &
                            -5.242446e+02_REAL64, &
                            -5.239946e+02_REAL64, &
                            -5.238973e+02_REAL64, &
                            -5.235840e+02_REAL64, &
                            -5.235061e+02_REAL64, &
                            -5.231265e+02_REAL64, &
                            -5.230709e+02_REAL64, &
                            -5.226227e+02_REAL64 &
                        ], &
                        [range(13, 21)], &
                        [94] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -1.925098e-03_REAL64, &
                            2.394221e-01_REAL64, &
                            -1.225697e-01_REAL64, &
                            -1.014450e-03_REAL64, &
                            7.730386e-04_REAL64, &
                            -8.426604e-02_REAL64, &
                            8.771404e-02_REAL64, &
                            8.211164e-04_REAL64 &
                        ], &
                        [range(1, 2), range(53, 56)], &
                        [2, 94] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=3_INT32, &
                    nspn=2_INT32, &
                    npty=0_INT32, &
                    nchan=2_INT32, &
                    nstat=94_INT32, &
                    nconat=[2_INT32], &
                    l2p=[2_INT32, 4_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            1.155858e+00_REAL64, &
                            -1.370679e+00_REAL64, &
                            -1.370679e+00_REAL64, &
                            -1.444823e-01_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(1, 2)], &
                        [2, 2, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            8.188337e+02_REAL64, &
                            1.145989e+03_REAL64, &
                            1.456651e+03_REAL64, &
                            3.806716e+03_REAL64, &
                            5.112383e+03_REAL64, &
                            1.360804e+04_REAL64, &
                            1.953941e+04_REAL64, &
                            6.868461e+04_REAL64, &
                            8.806403e+04_REAL64 &
                        ], &
                        [range(85, 93)], &
                        [94] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -6.835638e-03_REAL64, &
                            -1.565314e-04_REAL64, &
                            1.627709e-04_REAL64, &
                            -7.155816e-03_REAL64, &
                            2.733245e+00_REAL64, &
                            1.934096e-02_REAL64, &
                            -1.934809e-02_REAL64, &
                            2.733356e+00_REAL64 &
                        ], &
                        [range(1, 2), range(72, 75)], &
                        [2, 94] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=3_INT32, &
                    nspn=2_INT32, &
                    npty=1_INT32, &
                    nchan=3_INT32, &
                    nstat=139_INT32, &
                    nconat=[3_INT32], &
                    l2p=[1_INT32, 3_INT32, 5_INT32], &
                    cf=subarray( &
                        [ &
                            -1.589043e+00_REAL64, &
                            7.416758e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -9.010054e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(2, 3), range(2, 3)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.122731e+02_REAL64, &
                            -5.120809e+02_REAL64, &
                            -5.118013e+02_REAL64, &
                            -5.107591e+02_REAL64, &
                            -5.105286e+02_REAL64, &
                            -5.101048e+02_REAL64, &
                            -5.094631e+02_REAL64, &
                            -5.093578e+02_REAL64, &
                            -5.092617e+02_REAL64 &
                        ], &
                        [range(69, 77)], &
                        [139] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -6.227411e-02_REAL64, &
                            -3.628033e-01_REAL64, &
                            3.700676e-01_REAL64, &
                            -6.050409e-02_REAL64, &
                            3.635610e-04_REAL64, &
                            -8.007204e-03_REAL64, &
                            -2.797774e-02_REAL64, &
                            -3.665698e-01_REAL64 &
                        ], &
                        [range(1, 2), range(39, 42)], &
                        [3, 139] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/Ar+_H", expected)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_argon(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected

        expected = expected_H_file( &
            nelc=17_INT32, &
            nz=18_INT32, &
            lrang2=4_INT32, &
            lamax=4_INT32, &
            ntarg=2_INT32, &
            rmatr=20.0_REAL64, &
            bbloch=0.0_REAL64, &
            etarg=[-5.263857e+02_REAL64, -5.258874e+02_REAL64], &
            ltarg=[1_INT32, 0_INT32], &
            starg=[2_INT32, 2_INT32], &
            cfbut=RESHAPE( &
                [ &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00 &
                ], &
                [3, 4] &
            ), &
            L_blocks=[ &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=2_INT32, &
                    nstat=148_INT32, &
                    nconat=[1_INT32, 1_INT32], &
                    l2p=[1_INT32, 0_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -5.979063e-01_REAL64, &
                            -5.979063e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -2.236744e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(1, 2)], &
                        [2, 2, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.241843e+02_REAL64, &
                            -5.241465e+02_REAL64, &
                            -5.241011e+02_REAL64, &
                            -5.240867e+02_REAL64, &
                            -5.240504e+02_REAL64, &
                            -5.239989e+02_REAL64, &
                            -5.239654e+02_REAL64, &
                            -5.238841e+02_REAL64, &
                            -5.238749e+02_REAL64 &
                        ], &
                        [range(37, 45)], &
                        [148] &
                    ), &
                    wmat=subarray( &
                        [ &
                            6.693127e-03_REAL64, &
                            -1.476109e-02_REAL64, &
                            -6.486440e-02_REAL64, &
                            1.439075e-01_REAL64, &
                            -1.180994e-02_REAL64, &
                            -1.307103e-02_REAL64, &
                            -2.225462e-02_REAL64, &
                            1.893622e-02_REAL64 &
                        ], &
                        [range(1, 2), range(39, 42)], &
                        [2, 148] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=110_INT32, &
                    nconat=[1_INT32, 0_INT32], &
                    l2p=[1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            1.118372e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(1, 2)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.246004e+02_REAL64, &
                            -5.245732e+02_REAL64, &
                            -5.245450e+02_REAL64, &
                            -5.244286e+02_REAL64, &
                            -5.244158e+02_REAL64, &
                            -5.243851e+02_REAL64, &
                            -5.242823e+02_REAL64, &
                            -5.242558e+02_REAL64, &
                            -5.242525e+02_REAL64 &
                        ], &
                        [range(18, 26)], &
                        [110] &
                    ), &
                    wmat=subarray( &
                        [ &
                            1.604786e-02_REAL64, &
                            -1.109997e-03_REAL64, &
                            9.067899e-04_REAL64, &
                            -1.447316e-03_REAL64 &
                        ], &
                        [range(1, 1), range(56, 59)], &
                        [1, 110] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=3_INT32, &
                    nstat=236_INT32, &
                    nconat=[2_INT32, 1_INT32], &
                    l2p=[0_INT32, 2_INT32, 1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -4.881884e-01_REAL64, &
                            -4.881884e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -1.118372e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(2, 3), range(1, 2)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.249634e+02_REAL64, &
                            -5.249407e+02_REAL64, &
                            -5.249241e+02_REAL64, &
                            -5.248957e+02_REAL64, &
                            -5.248680e+02_REAL64, &
                            -5.248529e+02_REAL64, &
                            -5.248268e+02_REAL64, &
                            -5.248010e+02_REAL64, &
                            -5.247420e+02_REAL64 &
                        ], &
                        [range(33, 41)], &
                        [236] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -1.078607e-01_REAL64, &
                            7.997869e+00_REAL64, &
                            7.039908e-03_REAL64, &
                            1.011169e-02_REAL64, &
                            -2.416402e-09_REAL64, &
                            5.974540e-09_REAL64, &
                            1.037597e-07_REAL64, &
                            3.325217e-04_REAL64 &
                        ], &
                        [range(1, 2), range(218, 221)], &
                        [3, 236] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=3_INT32, &
                    nstat=253_INT32, &
                    nconat=[2_INT32, 1_INT32], &
                    l2p=[1_INT32, 3_INT32, 2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -3.781491e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            -4.631362e-01_REAL64, &
                            -1.643665e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            -8.946977e-01_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(1, 2), range(1, 2)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -3.535809e+02_REAL64, &
                            -3.148299e+02_REAL64, &
                            -2.698590e+02_REAL64, &
                            -2.592180e+02_REAL64, &
                            -1.837871e+02_REAL64, &
                            -8.685873e+01_REAL64, &
                            -8.046149e+01_REAL64, &
                            8.187016e+01_REAL64, &
                            2.942652e+02_REAL64 &
                        ], &
                        [range(224, 232)], &
                        [253] &
                    ), &
                    wmat=subarray( &
                        [ &
                            9.652977e-02_REAL64, &
                            -2.955731e-02_REAL64, &
                            -9.598501e-03_REAL64, &
                            3.097445e-01_REAL64, &
                            -2.765305e-02_REAL64, &
                            -2.408734e-03_REAL64, &
                            7.583821e-03_REAL64, &
                            -5.794324e-02_REAL64 &
                        ], &
                        [range(1, 2), range(26, 29)], &
                        [3, 253] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=1_INT32, &
                    nstat=127_INT32, &
                    nconat=[1_INT32, 0_INT32], &
                    l2p=[2_INT32], &
                    cf=subarray( &
                        [ &
                            1.118372e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(2, 3)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.136277e+02_REAL64, &
                            -5.123365e+02_REAL64, &
                            -5.119498e+02_REAL64, &
                            -5.103614e+02_REAL64, &
                            -5.081552e+02_REAL64, &
                            -5.053738e+02_REAL64, &
                            -5.039837e+02_REAL64, &
                            -5.018168e+02_REAL64, &
                            -4.972013e+02_REAL64 &
                        ], &
                        [range(101, 109)], &
                        [127] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -6.459385e-04_REAL64, &
                            -3.932961e-04_REAL64, &
                            -4.665047e-04_REAL64, &
                            -1.239182e-02_REAL64 &
                        ], &
                        [range(1, 1), range(118, 121)], &
                        [1, 127] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/Ar_H", expected)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_helium(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected

        expected = expected_H_file( &
            nelc=1_INT32, &
            nz=2_INT32, &
            lrang2=5_INT32, &
            lamax=4_INT32, &
            ntarg=3_INT32, &
            rmatr=15.0_REAL64, &
            bbloch=0.0_REAL64, &
            etarg=[-2.000000e+00_REAL64, 4.914448e-09_REAL64, 6.666667e-01_REAL64], &
            ltarg=[0_INT32, 1_INT32, 0_INT32], &
            starg=[2_INT32, 2_INT32, 2_INT32], &
            cfbut=RESHAPE( &
                [ &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00 &
                ], &
                [3, 5] &
            ), &
            L_blocks=[ &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=3_INT32, &
                    nstat=118_INT32, &
                    nconat=[1_INT32, 1_INT32, 1_INT32], &
                    l2p=[0_INT32, 1_INT32, 0_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(1, 2), range(3, 4)], &
                        [3, 3, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            1.301149e-01_REAL64, &
                            3.138413e-01_REAL64, &
                            5.160719e-01_REAL64, &
                            5.615779e-01_REAL64, &
                            5.736477e-01_REAL64, &
                            6.331484e-01_REAL64, &
                            7.632393e-01_REAL64, &
                            8.443597e-01_REAL64, &
                            9.578881e-01_REAL64 &
                        ], &
                        [range(16, 24)], &
                        [118] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -2.213480e-02_REAL64, &
                            -3.481964e-01_REAL64, &
                            -4.793013e-02_REAL64, &
                            7.859863e-02_REAL64, &
                            -5.138573e-01_REAL64, &
                            -5.182887e-02_REAL64, &
                            -7.185414e-02_REAL64, &
                            3.487884e-01_REAL64 &
                        ], &
                        [range(2, 3), range(58, 61)], &
                        [3, 118] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=38_INT32, &
                    nconat=[0_INT32, 1_INT32, 0_INT32], &
                    l2p=[1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(3, 4)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            4.564805e+00_REAL64, &
                            5.242328e+00_REAL64, &
                            5.969256e+00_REAL64, &
                            6.728598e+00_REAL64, &
                            7.595879e+00_REAL64, &
                            8.682067e+00_REAL64, &
                            9.403876e+00_REAL64, &
                            1.127018e+01_REAL64, &
                            1.367212e+01_REAL64 &
                        ], &
                        [range(14, 22)], &
                        [38] &
                    ), &
                    wmat=subarray( &
                        [ &
                            3.754020e-01_REAL64, &
                            -3.736676e-01_REAL64, &
                            3.722773e-01_REAL64, &
                            -3.711758e-01_REAL64 &
                        ], &
                        [range(1, 1), range(10, 13)], &
                        [1, 38] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=1_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=4_INT32, &
                    nstat=154_INT32, &
                    nconat=[1_INT32, 2_INT32, 1_INT32], &
                    l2p=[1_INT32, 0_INT32, 2_INT32, 1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            1.060660e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(3, 4), range(2, 3)], &
                        [4, 4, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            7.353179e-01_REAL64, &
                            7.362557e-01_REAL64, &
                            8.217475e-01_REAL64, &
                            8.850798e-01_REAL64, &
                            9.328590e-01_REAL64, &
                            1.036696e+00_REAL64, &
                            1.112085e+00_REAL64, &
                            1.203377e+00_REAL64, &
                            1.380755e+00_REAL64 &
                        ], &
                        [range(25, 33)], &
                        [154] &
                    ), &
                    wmat=subarray( &
                        [ &
                            2.012439e-07_REAL64, &
                            -5.250795e-08_REAL64, &
                            -7.121967e-08_REAL64, &
                            -1.936213e-03_REAL64, &
                            -8.593458e-08_REAL64, &
                            5.282430e-03_REAL64, &
                            -4.567355e-03_REAL64, &
                            -2.715994e-08_REAL64 &
                        ], &
                        [range(3, 4), range(139, 142)], &
                        [4, 154] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=4_INT32, &
                    nstat=152_INT32, &
                    nconat=[1_INT32, 2_INT32, 1_INT32], &
                    l2p=[2_INT32, 1_INT32, 3_INT32, 2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(3, 4)], &
                        [4, 4, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            5.226708e+00_REAL64, &
                            5.324347e+00_REAL64, &
                            5.372493e+00_REAL64, &
                            5.651319e+00_REAL64, &
                            6.022364e+00_REAL64, &
                            6.096928e+00_REAL64, &
                            6.281770e+00_REAL64, &
                            6.353784e+00_REAL64, &
                            6.778388e+00_REAL64 &
                        ], &
                        [range(61, 69)], &
                        [152] &
                    ), &
                    wmat=subarray( &
                        [ &
                            3.456141e-01_REAL64, &
                            4.777838e-02_REAL64, &
                            2.242844e-02_REAL64, &
                            -5.335526e-01_REAL64, &
                            5.711070e-03_REAL64, &
                            7.261338e-02_REAL64, &
                            4.986875e-03_REAL64, &
                            1.796685e-01_REAL64 &
                        ], &
                        [range(3, 4), range(81, 84)], &
                        [4, 152] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=1_INT32, &
                    nstat=38_INT32, &
                    nconat=[0_INT32, 1_INT32, 0_INT32], &
                    l2p=[2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            -7.500000e-01_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(1, 2)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            9.836810e-02_REAL64, &
                            2.555221e-01_REAL64, &
                            4.590305e-01_REAL64, &
                            7.081800e-01_REAL64, &
                            1.002686e+00_REAL64, &
                            1.342360e+00_REAL64, &
                            1.727019e+00_REAL64, &
                            2.156480e+00_REAL64, &
                            2.630576e+00_REAL64 &
                        ], &
                        [range(3, 11)], &
                        [38] &
                    ), &
                    wmat=subarray( &
                        [ &
                            3.104622e-01_REAL64, &
                            4.197779e-01_REAL64, &
                            -3.927771e-01_REAL64, &
                            3.823778e-01_REAL64 &
                        ], &
                        [range(1, 1), range(1, 4)], &
                        [1, 38] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=3_INT32, &
                    nspn=1_INT32, &
                    npty=0_INT32, &
                    nchan=1_INT32, &
                    nstat=37_INT32, &
                    nconat=[0_INT32, 1_INT32, 0_INT32], &
                    l2p=[3_INT32], &
                    cf=subarray( &
                        [ &
                            -7.500000e-01_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 1), range(1, 1), range(2, 3)], &
                        [1, 1, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            4.036264e+00_REAL64, &
                            4.665850e+00_REAL64, &
                            5.340488e+00_REAL64, &
                            6.064906e+00_REAL64, &
                            6.820544e+00_REAL64, &
                            7.700506e+00_REAL64, &
                            8.762154e+00_REAL64, &
                            9.503219e+00_REAL64, &
                            1.140512e+01_REAL64 &
                        ], &
                        [range(13, 21)], &
                        [37] &
                    ), &
                    wmat=subarray( &
                        [ &
                            7.004844e+00_REAL64, &
                            4.331352e-02_REAL64, &
                            -7.319062e-03_REAL64, &
                            3.995422e-03_REAL64 &
                        ], &
                        [range(1, 1), range(33, 36)], &
                        [1, 37] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=3_INT32, &
                    nspn=1_INT32, &
                    npty=1_INT32, &
                    nchan=4_INT32, &
                    nstat=148_INT32, &
                    nconat=[1_INT32, 2_INT32, 1_INT32], &
                    l2p=[3_INT32, 2_INT32, 4_INT32, 3_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(3, 4), range(1, 2), range(3, 4)], &
                        [4, 4, 4] &
                    ), &
                    eig=subarray( &
                        [ &
                            2.689251e+01_REAL64, &
                            2.692554e+01_REAL64, &
                            2.716443e+01_REAL64, &
                            2.766509e+01_REAL64, &
                            3.128056e+01_REAL64, &
                            3.216999e+01_REAL64, &
                            3.681019e+01_REAL64, &
                            3.929200e+01_REAL64, &
                            4.629220e+01_REAL64 &
                        ], &
                        [range(100, 108)], &
                        [148] &
                    ), &
                    wmat=subarray( &
                        [ &
                            2.230332e-08_REAL64, &
                            -2.712256e-08_REAL64, &
                            -2.058155e-10_REAL64, &
                            3.979060e-03_REAL64, &
                            -2.749652e-03_REAL64, &
                            -8.091576e-10_REAL64, &
                            -9.823846e-10_REAL64, &
                            8.364450e-10_REAL64 &
                        ], &
                        [range(2, 3), range(143, 146)], &
                        [4, 148] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/He_H", expected)
    END SUBROUTINE

    SUBROUTINE test_RMatrixI_example(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected

        ! NOTE: This test is based on data that has not been confirmed to be inputs to a successful calculation, therefore some values may be non-sensical or incorrect.
        expected = expected_H_file( &
            nelc=17_INT32, &
            nz=18_INT32, &
            lrang2=11_INT32, &
            lamax=8_INT32, &
            ntarg=2_INT32, &
            rmatr=20.0_REAL64, &
            bbloch=0.0_REAL64, &
            etarg=[-5.262749e+02_REAL64, -5.262684e+02_REAL64], &
            ltarg=[3_INT32, 1_INT32], &
            starg=[1_INT32, 1_INT32], &
            cfbut=RESHAPE( &
                [ &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00, &
                    0.000000e+00 &
                ], &
                [3, 11] &
            ), &
            L_blocks=[ &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=0_INT32, &
                    npty=0_INT32, &
                    nchan=2_INT32, &
                    nstat=97_INT32, &
                    nconat=[1_INT32, 1_INT32], &
                    l2p=[1_INT32, 1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(6, 7)], &
                        [2, 2, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.251963e+02_REAL64, &
                            -5.250755e+02_REAL64, &
                            -5.250210e+02_REAL64, &
                            -5.250129e+02_REAL64, &
                            -5.247774e+02_REAL64, &
                            -5.247495e+02_REAL64, &
                            -5.244726e+02_REAL64, &
                            -5.244480e+02_REAL64, &
                            -5.241410e+02_REAL64 &
                        ], &
                        [range(22, 30)], &
                        [97] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -1.616677e-04_REAL64, &
                            -9.343850e-05_REAL64, &
                            9.350592e-05_REAL64, &
                            -1.617466e-04_REAL64, &
                            5.691837e+00_REAL64, &
                            9.507776e-02_REAL64, &
                            9.507782e-02_REAL64, &
                            -5.691837e+00_REAL64 &
                        ], &
                        [range(1, 2), range(82, 85)], &
                        [2, 97] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=0_INT32, &
                    nspn=0_INT32, &
                    npty=1_INT32, &
                    nchan=2_INT32, &
                    nstat=104_INT32, &
                    nconat=[1_INT32, 1_INT32], &
                    l2p=[2_INT32, 0_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(6, 7)], &
                        [2, 2, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.206204e+02_REAL64, &
                            -5.205391e+02_REAL64, &
                            -5.200470e+02_REAL64, &
                            -5.199735e+02_REAL64, &
                            -5.194111e+02_REAL64, &
                            -5.193448e+02_REAL64, &
                            -5.187138e+02_REAL64, &
                            -5.186643e+02_REAL64, &
                            -5.181646e+02_REAL64 &
                        ], &
                        [range(54, 62)], &
                        [104] &
                    ), &
                    wmat=subarray( &
                        [ &
                            9.217239e-03_REAL64, &
                            -3.346594e-01_REAL64, &
                            3.208359e-01_REAL64, &
                            1.032508e-02_REAL64, &
                            1.115861e-02_REAL64, &
                            -3.316763e-01_REAL64, &
                            -3.060921e-01_REAL64, &
                            -8.643072e-03_REAL64 &
                        ], &
                        [range(1, 2), range(16, 19)], &
                        [2, 104] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=0_INT32, &
                    npty=0_INT32, &
                    nchan=5_INT32, &
                    nstat=239_INT32, &
                    nconat=[3_INT32, 2_INT32], &
                    l2p=[1_INT32, 1_INT32, 3_INT32, 1_INT32, 1_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 2), range(1, 2), range(6, 7)], &
                        [5, 5, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.248012e+02_REAL64, &
                            -5.247840e+02_REAL64, &
                            -5.247782e+02_REAL64, &
                            -5.247758e+02_REAL64, &
                            -5.247087e+02_REAL64, &
                            -5.244958e+02_REAL64, &
                            -5.244806e+02_REAL64, &
                            -5.244741e+02_REAL64, &
                            -5.244717e+02_REAL64 &
                        ], &
                        [range(61, 69)], &
                        [239] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -6.823529e-03_REAL64, &
                            -2.313146e-01_REAL64, &
                            2.855112e-01_REAL64, &
                            -4.879779e-03_REAL64, &
                            4.487677e-06_REAL64, &
                            -4.040306e-02_REAL64, &
                            -1.527474e-01_REAL64, &
                            2.405293e-05_REAL64 &
                        ], &
                        [range(4, 5), range(78, 81)], &
                        [5, 239] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=2_INT32, &
                    nspn=0_INT32, &
                    npty=1_INT32, &
                    nchan=5_INT32, &
                    nstat=264_INT32, &
                    nconat=[3_INT32, 2_INT32], &
                    l2p=[2_INT32, 0_INT32, 2_INT32, 0_INT32, 2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(1, 2), range(5, 6)], &
                        [5, 5, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.068641e+02_REAL64, &
                            -5.068220e+02_REAL64, &
                            -5.061160e+02_REAL64, &
                            -5.061077e+02_REAL64, &
                            -5.021183e+02_REAL64, &
                            -5.020897e+02_REAL64, &
                            -5.020380e+02_REAL64, &
                            -5.006536e+02_REAL64, &
                            -5.006446e+02_REAL64 &
                        ], &
                        [range(194, 202)], &
                        [264] &
                    ), &
                    wmat=subarray( &
                        [ &
                            2.527084e-02_REAL64, &
                            -1.164495e-01_REAL64, &
                            2.594995e-02_REAL64, &
                            -2.408005e-01_REAL64, &
                            8.112118e-03_REAL64, &
                            1.492221e-01_REAL64, &
                            3.628755e-01_REAL64, &
                            4.274920e-02_REAL64 &
                        ], &
                        [range(2, 3), range(150, 153)], &
                        [5, 264] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=4_INT32, &
                    nspn=0_INT32, &
                    npty=0_INT32, &
                    nchan=6_INT32, &
                    nstat=286_INT32, &
                    nconat=[4_INT32, 2_INT32], &
                    l2p=[1_INT32, 3_INT32, 1_INT32, 3_INT32, 1_INT32, 3_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(5, 6), range(3, 4)], &
                        [6, 6, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            1.455918e+04_REAL64, &
                            1.455918e+04_REAL64, &
                            1.455919e+04_REAL64, &
                            7.475342e+04_REAL64, &
                            7.475343e+04_REAL64, &
                            7.475344e+04_REAL64, &
                            5.352679e+05_REAL64, &
                            5.352682e+05_REAL64, &
                            5.352682e+05_REAL64 &
                        ], &
                        [range(278, 286)], &
                        [286] &
                    ), &
                    wmat=subarray( &
                        [ &
                            1.108534e-01_REAL64, &
                            1.693121e-02_REAL64, &
                            -2.740931e-01_REAL64, &
                            -1.160877e-02_REAL64, &
                            -1.342780e-01_REAL64, &
                            1.497043e-02_REAL64, &
                            -8.938210e-03_REAL64, &
                            2.556429e-01_REAL64 &
                        ], &
                        [range(1, 2), range(80, 83)], &
                        [6, 286] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl=4_INT32, &
                    nspn=0_INT32, &
                    npty=1_INT32, &
                    nchan=6_INT32, &
                    nstat=318_INT32, &
                    nconat=[4_INT32, 2_INT32], &
                    l2p=[0_INT32, 2_INT32, 2_INT32, 4_INT32, 2_INT32, 2_INT32], &
                    cf=subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 3), range(5, 6), range(7, 8)], &
                        [6, 6, 8] &
                    ), &
                    eig=subarray( &
                        [ &
                            -5.264249e+02_REAL64, &
                            -5.263408e+02_REAL64, &
                            -5.263381e+02_REAL64, &
                            -5.263351e+02_REAL64, &
                            -5.263299e+02_REAL64, &
                            -5.263294e+02_REAL64, &
                            -5.263133e+02_REAL64, &
                            -5.263094e+02_REAL64, &
                            -5.263043e+02_REAL64 &
                        ], &
                        [range(1, 9)], &
                        [318] &
                    ), &
                    wmat=subarray( &
                        [ &
                            -4.335756e-06_REAL64, &
                            1.523971e-01_REAL64, &
                            3.181784e-05_REAL64, &
                            3.017985e-02_REAL64, &
                            2.074737e-06_REAL64, &
                            -3.774609e-03_REAL64, &
                            -5.983398e-03_REAL64, &
                            -4.330058e-05_REAL64 &
                        ], &
                        [range(4, 5), range(89, 92)], &
                        [6, 318] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/RM1_H", expected)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_iodine(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        TYPE(expected_H_file) :: expected
        REAL(REAL64), DIMENSION(3, 11) :: cfbut

        cfbut = 0.0_REAL64

        expected = expected_H_file( &
            nelc = 52_INT32, &
            nz = 53_INT32, &
            lrang2 = 11_INT32, &
            lamax = 10_INT32, &
            ntarg = 3_INT32, &
            rmatr = 20.0_REAL64, &
            bbloch = 0.0_REAL64, &
            etarg = [-6.917447e+03_REAL64, -6.917418e+03_REAL64, -6.917415e+03_REAL64], &
            ltarg = [4_INT32, 0_INT32, 2_INT32], &
            starg = [0_INT32, 0_INT32, 0_INT32], &
            cfbut = cfbut, &
            L_blocks = [ &
                expected_L_block( &
                    lrgl = 1_INT32, &
                    nspn = 0_INT32, &
                    npty = 0_INT32, &
                    nchan = 5_INT32, &
                    nstat = 220_INT32, &
                    nconat = [2_INT32, 1_INT32, 2_INT32], &
                    l2p = [2_INT32, 2_INT32, 0_INT32, 0_INT32, 2_INT32], &
                    cf = subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            -5.551115e-16_REAL64, &
                            1.027011e+00_REAL64, &
                            -4.440892e-16_REAL64, &
                            3.448074e+00_REAL64, &
                            -4.440892e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            8.881784e-16_REAL64, &
                            -2.986120e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 3), range(2, 4), range(1, 3)], &
                        [5, 5, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -5.379588e+03_REAL64, &
                            -5.379583e+03_REAL64, &
                            -4.804154e+03_REAL64, &
                            -4.804124e+03_REAL64, &
                            -4.746361e+03_REAL64, &
                            1.198378e+03_REAL64, &
                            1.198382e+03_REAL64, &
                            1.849458e+03_REAL64, &
                            1.849459e+03_REAL64, &
                            1.849489e+03_REAL64, &
                            3.798580e+04_REAL64 &
                        ], &
                        [range(202, 212)], &
                        [220] &
                    ), &
                    wmat = subarray( &
                        [ &
                            3.060612e-05_REAL64, &
                            -1.489033e-02_REAL64, &
                            -7.993910e-04_REAL64, &
                            1.394913e+00_REAL64, &
                            2.384579e-02_REAL64, &
                            -5.297745e-05_REAL64, &
                            -8.253881e-03_REAL64, &
                            7.718192e-05_REAL64, &
                            2.374771e-02_REAL64, &
                            -1.394678e+00_REAL64, &
                            5.528775e-04_REAL64, &
                            -2.749575e-04_REAL64, &
                            -1.249636e-02_REAL64, &
                            -6.100056e-03_REAL64, &
                            -1.458933e-04_REAL64, &
                            -2.057701e-04_REAL64, &
                            -4.439734e-04_REAL64, &
                            6.093929e-03_REAL64, &
                            -1.248731e-02_REAL64, &
                            -2.717382e-04_REAL64, &
                            3.905529e-03_REAL64, &
                            -7.010017e-03_REAL64, &
                            1.984739e-04_REAL64, &
                            8.781237e-05_REAL64, &
                            8.935985e-03_REAL64 &
                        ], &
                        [range(1, 5), range(162, 166)], &
                        [5, 220] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl = 1_INT32, &
                    nspn = 0_INT32, &
                    npty = 1_INT32, &
                    nchan = 5_INT32, &
                    nstat = 221_INT32, &
                    nconat = [2_INT32, 1_INT32, 2_INT32], &
                    l2p = [1_INT32, 3_INT32, 1_INT32, 1_INT32, 1_INT32], &
                    cf = subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            1.643217e+00_REAL64, &
                            2.670867e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            2.670867e+00_REAL64, &
                            -2.220446e-16_REAL64, &
                            -1.110223e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            -1.110223e-16_REAL64, &
                            -2.516445e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(2, 4), range(2, 4), range(1, 3)], &
                        [5, 5, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -5.853580e+03_REAL64, &
                            -5.853548e+03_REAL64, &
                            -5.681973e+03_REAL64, &
                            -2.985310e+03_REAL64, &
                            -2.985308e+03_REAL64, &
                            -2.611612e+03_REAL64, &
                            -2.611580e+03_REAL64, &
                            -2.296402e+03_REAL64, &
                            1.050106e+04_REAL64, &
                            1.050106e+04_REAL64, &
                            1.401304e+04_REAL64 &
                        ], &
                        [range(202, 212)], &
                        [221] &
                    ), &
                    wmat = subarray( &
                        [ &
                            -5.130822e-03_REAL64, &
                            -2.709821e-05_REAL64, &
                            -1.192412e-04_REAL64, &
                            3.259812e-03_REAL64, &
                            2.227279e-03_REAL64, &
                            3.946352e-03_REAL64, &
                            -1.635830e-05_REAL64, &
                            1.226078e-04_REAL64, &
                            4.054129e-03_REAL64, &
                            3.152520e-03_REAL64, &
                            -2.951503e-06_REAL64, &
                            1.740847e-02_REAL64, &
                            -2.313025e-05_REAL64, &
                            -3.258726e-06_REAL64, &
                            1.783839e-05_REAL64, &
                            2.353353e+00_REAL64, &
                            -5.309132e-03_REAL64, &
                            -1.770818e-02_REAL64, &
                            2.945308e-03_REAL64, &
                            -1.904930e-02_REAL64, &
                            -5.193172e-03_REAL64, &
                            -2.353742e+00_REAL64, &
                            4.307666e-02_REAL64, &
                            3.204322e-03_REAL64, &
                            -2.593378e-02_REAL64 &
                        ], &
                        [range(1, 5), range(172, 176)], &
                        [5, 221] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl = 3_INT32, &
                    nspn = 0_INT32, &
                    npty = 0_INT32, &
                    nchan = 8_INT32, &
                    nstat = 352_INT32, &
                    nconat = [4_INT32, 1_INT32, 3_INT32], &
                    l2p = [2_INT32, 0_INT32, 2_INT32, 4_INT32, 2_INT32, 0_INT32, 2_INT32, 2_INT32], &
                    cf = subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            2.472742e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            1.665335e-15_REAL64, &
                            -1.110223e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            -1.110223e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            -1.779395e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(5, 7), range(4, 6), range(1, 3)], &
                        [8, 8, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -4.746360e+03_REAL64, &
                            -4.746330e+03_REAL64, &
                            6.701936e+02_REAL64, &
                            1.198349e+03_REAL64, &
                            1.198381e+03_REAL64, &
                            1.849457e+03_REAL64, &
                            1.849459e+03_REAL64, &
                            1.849487e+03_REAL64, &
                            1.849490e+03_REAL64, &
                            1.849491e+03_REAL64, &
                            3.798577e+04_REAL64 &
                        ], &
                        [range(332, 342)], &
                        [352] &
                    ), &
                    wmat = subarray( &
                        [ &
                            -1.833080e-09_REAL64, &
                            9.197177e-04_REAL64, &
                            5.770931e-10_REAL64, &
                            2.228275e-08_REAL64, &
                            -5.828379e-09_REAL64, &
                            9.233253e-10_REAL64, &
                            3.552670e-05_REAL64, &
                            -1.317689e-09_REAL64, &
                            6.213957e-08_REAL64, &
                            -4.632180e-09_REAL64, &
                            -5.135653e-17_REAL64, &
                            -5.915861e-11_REAL64, &
                            -9.705761e-04_REAL64, &
                            -1.189457e-10_REAL64, &
                            1.648305e-17_REAL64, &
                            8.051957e-04_REAL64, &
                            -6.117321e-10_REAL64, &
                            -8.054056e-17_REAL64, &
                            -8.073711e-10_REAL64, &
                            9.472698e-06_REAL64, &
                            9.472568e-06_REAL64, &
                            2.686951e-10_REAL64, &
                            -2.462500e-17_REAL64, &
                            2.050898e-10_REAL64, &
                            -8.051958e-04_REAL64 &
                        ], &
                        [range(2, 6), range(332, 336)], &
                        [8, 352] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl = 3_INT32, &
                    nspn = 0_INT32, &
                    npty = 1_INT32, &
                    nchan = 8_INT32, &
                    nstat = 353_INT32, &
                    nconat = [4_INT32, 1_INT32, 3_INT32], &
                    l2p = [1_INT32, 3_INT32, 1_INT32, 3_INT32, 1_INT32, 1_INT32, 1_INT32, 3_INT32], &
                    cf = subarray( &
                        [ &
                            2.832882e+00_REAL64, &
                            -2.313038e+00_REAL64, &
                            1.249001e-16_REAL64, &
                            -2.151057e-16_REAL64, &
                            -2.220446e-16_REAL64, &
                            -1.266903e+00_REAL64, &
                            -2.775558e-17_REAL64, &
                            1.110223e-16_REAL64, &
                            1.034422e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(1, 3), range(6, 8), range(2, 4)], &
                        [8, 8, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -2.985340e+03_REAL64, &
                            -2.985307e+03_REAL64, &
                            -2.611611e+03_REAL64, &
                            -2.611584e+03_REAL64, &
                            -2.611580e+03_REAL64, &
                            -2.296403e+03_REAL64, &
                            -2.296401e+03_REAL64, &
                            -2.296372e+03_REAL64, &
                            1.050103e+04_REAL64, &
                            1.050107e+04_REAL64, &
                            1.401304e+04_REAL64 &
                        ], &
                        [range(331, 341)], &
                        [353] &
                    ), &
                    wmat = subarray( &
                        [ &
                            -1.290034e-04_REAL64, &
                            2.758139e-03_REAL64, &
                            -4.902021e-05_REAL64, &
                            2.579397e-02_REAL64, &
                            3.059210e-01_REAL64, &
                            -4.609419e-02_REAL64, &
                            -4.860993e-05_REAL64, &
                            3.318142e-01_REAL64, &
                            -2.809744e-04_REAL64, &
                            -3.038003e-04_REAL64, &
                            -3.412206e-01_REAL64, &
                            2.146952e-04_REAL64, &
                            -4.134776e-02_REAL64, &
                            -7.465608e-04_REAL64, &
                            3.458805e-04_REAL64, &
                            1.429465e-02_REAL64, &
                            -3.744929e-04_REAL64, &
                            8.311495e-02_REAL64, &
                            -2.763011e-04_REAL64, &
                            -3.997693e-04_REAL64, &
                            -5.187352e-05_REAL64, &
                            -2.637378e-01_REAL64, &
                            -2.746562e-04_REAL64, &
                            -2.954969e-02_REAL64, &
                            -3.659417e-02_REAL64 &
                        ], &
                        [range(2, 6), range(172, 176)], &
                        [8, 353] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl = 5_INT32, &
                    nspn = 0_INT32, &
                    npty = 0_INT32, &
                    nchan = 9_INT32, &
                    nstat = 396_INT32, &
                    nconat = [5_INT32, 1_INT32, 3_INT32], &
                    l2p = [0_INT32, 2_INT32, 4_INT32, 2_INT32, 4_INT32, 2_INT32, 2_INT32, 2_INT32, 4_INT32], &
                    cf = subarray( &
                        [ &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            9.992007e-16_REAL64, &
                            -2.336522e+00_REAL64, &
                            2.023487e+00_REAL64, &
                            -1.304512e-15_REAL64, &
                            -1.942890e-16_REAL64, &
                            -3.330669e-16_REAL64, &
                            1.258223e+00_REAL64, &
                            -3.885781e-16_REAL64, &
                            5.551115e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(7, 9), range(5, 7), range(1, 3)], &
                        [9, 9, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -4.746334e+03_REAL64, &
                            -4.746330e+03_REAL64, &
                            6.701919e+02_REAL64, &
                            6.701943e+02_REAL64, &
                            6.702236e+02_REAL64, &
                            1.198349e+03_REAL64, &
                            1.849457e+03_REAL64, &
                            1.849458e+03_REAL64, &
                            1.849487e+03_REAL64, &
                            1.849490e+03_REAL64, &
                            1.849491e+03_REAL64 &
                        ], &
                        [range(378, 388)], &
                        [396] &
                    ), &
                    wmat = subarray( &
                        [ &
                            -3.870728e-02_REAL64, &
                            1.737070e-02_REAL64, &
                            3.005162e-01_REAL64, &
                            4.111988e-02_REAL64, &
                            -1.231660e-03_REAL64, &
                            -7.629097e-02_REAL64, &
                            -2.236665e-03_REAL64, &
                            -6.370258e-02_REAL64, &
                            2.639242e-01_REAL64, &
                            -3.432759e-02_REAL64, &
                            -2.500820e-01_REAL64, &
                            -3.258053e-04_REAL64, &
                            -9.037324e-03_REAL64, &
                            -4.713978e-02_REAL64, &
                            -1.735194e-01_REAL64, &
                            -8.671477e-02_REAL64, &
                            -4.798060e-02_REAL64, &
                            6.572192e-03_REAL64, &
                            1.195346e-01_REAL64, &
                            1.995382e-01_REAL64, &
                            -1.516250e-01_REAL64, &
                            -1.679631e-03_REAL64, &
                            -1.758565e-02_REAL64, &
                            -1.294193e-01_REAL64, &
                            1.848112e-01_REAL64 &
                        ], &
                        [range(3, 7), range(81, 85)], &
                        [9, 396] &
                    ) &
                ), &
                expected_L_block( &
                    lrgl = 5_INT32, &
                    nspn = 0_INT32, &
                    npty = 1_INT32, &
                    nchan = 9_INT32, &
                    nstat = 396_INT32, &
                    nconat = [5_INT32, 1_INT32, 3_INT32], &
                    l2p = [1_INT32, 3_INT32, 1_INT32, 3_INT32, 5_INT32, 3_INT32, 1_INT32, 3_INT32, 3_INT32], &
                    cf = subarray( &
                        [ &
                            2.379397e+00_REAL64, &
                            1.443290e-15_REAL64, &
                            6.661338e-16_REAL64, &
                            0.000000e+00_REAL64, &
                            6.661338e-16_REAL64, &
                            -2.516445e-01_REAL64, &
                            0.000000e+00_REAL64, &
                            3.330669e-16_REAL64, &
                            -1.849202e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64, &
                            0.000000e+00_REAL64 &
                        ], &
                        [range(5, 7), range(6, 8), range(2, 4)], &
                        [9, 9, 10] &
                    ), &
                    eig = subarray( &
                        [ &
                            -2.985340e+03_REAL64, &
                            -2.611614e+03_REAL64, &
                            -2.611581e+03_REAL64, &
                            -2.296404e+03_REAL64, &
                            -2.296401e+03_REAL64, &
                            -2.296374e+03_REAL64, &
                            -2.296371e+03_REAL64, &
                            -2.296369e+03_REAL64, &
                            4.398734e+03_REAL64, &
                            1.050103e+04_REAL64, &
                            1.401304e+04_REAL64 &
                        ], &
                        [range(379, 389)], &
                        [396] &
                    ), &
                    wmat = subarray( &
                        [ &
                            5.936569e-04_REAL64, &
                            3.134588e-02_REAL64, &
                            -5.487661e-04_REAL64, &
                            -6.647120e-03_REAL64, &
                            -1.168082e-04_REAL64, &
                            4.107297e-04_REAL64, &
                            1.642561e-02_REAL64, &
                            1.106654e-03_REAL64, &
                            1.652692e-02_REAL64, &
                            1.687426e-04_REAL64, &
                            -4.199910e-02_REAL64, &
                            3.637355e-04_REAL64, &
                            6.100232e-02_REAL64, &
                            -5.068793e-04_REAL64, &
                            -1.165709e-06_REAL64, &
                            -5.955470e-02_REAL64, &
                            1.165850e-04_REAL64, &
                            -4.190124e-02_REAL64, &
                            4.163360e-04_REAL64, &
                            1.868202e-06_REAL64, &
                            1.669771e-02_REAL64, &
                            -1.506691e-03_REAL64, &
                            7.668193e-03_REAL64, &
                            -1.601212e-05_REAL64, &
                            -7.549328e-08_REAL64 &
                        ], &
                        [range(1, 5), range(243, 247)], &
                        [9, 396] &
                    ) &
                ) &
            ] &
        )

        CALL test_H_file(error, "unit_test_data/I_H", expected)
    END SUBROUTINE
END MODULE
