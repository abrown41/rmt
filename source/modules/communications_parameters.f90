! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Defines all parameters to be used in parallel communication, specifically the
!> numbers of processing elements to use in each region, the location of master
!> cores and MPI communicators.

MODULE communications_parameters

    IMPLICIT NONE

    ! In MPI_COMM_WORLD:
    ! pes rank (pe_id_1st) to (pe_id_1st + no_of_pes_to_use_inner - 1) are in INNER REGION
    ! PEs rank (pe_id_1st + no_of_pes_to_use_inner) to (pe_id_last) are in OUTER REGION
    ! In MPI_COMM_REGION:
    ! Inner Region: PEs rank (pe_id_1st_inner) to (pe_id_last_inner)
    ! Outer Region: PEs rank (pe_id_1st_outer) to (pe_id_last_outer)

    ! Define how many processors to work in each of the inner and outer regions:
    ! no_of_pes_to_use_inner must be some multiple of number of symmetry blocks (see L_max in Hugo's code)
!   INTEGER, PARAMETER :: no_of_pes_to_use_inner = 82
!   INTEGER, PARAMETER :: no_of_pes_to_use_outer = 46
    ! Define how many processors to use in total

    ! Do not change any of the parameters below:
    INTEGER, PARAMETER :: pe_id_1st = 0
    INTEGER, PARAMETER :: pe_id_1st_inner = 0
    INTEGER, PARAMETER :: pe_id_1st_outer = 0
    INTEGER, PARAMETER :: pe_id_1st_double_outer = 0

    INTEGER            :: pe_id_last                 != no_of_pes_to_use + pe_id_1st - 1
    INTEGER            :: pe_id_last_inner           != no_of_pes_to_use_inner + pe_id_1st_inner - 1
    INTEGER            :: pe_id_last_outer           != no_of_pes_to_use_outer + pe_id_1st_outer - 1
    INTEGER            :: pe_id_last_double_outer    != no_of_pes_to_use_double_outer + pe_id_1st_double_outer - 1
    INTEGER            :: id_of_1st_pe_outer         != pe_id_last_inner + 1
    INTEGER            :: id_of_last_pe_outer        != pe_id_last_inner + pe_id_last_outer + 1
    INTEGER            :: id_of_last_pe_double_outer != pe_id_last_outer + pe_id_last_double_outer + 1

    INTEGER, PARAMETER :: block_id_1st = 0
    INTEGER            :: block_id_last      != no_of_pes_to_use_outer / no_of_pes_per_sector - 1
    INTEGER            :: no_of_blocks       != (block_id_last - block_id_1st + 1) 
    ! NB Currently read_my_wavefunc and write_my_wavefunc require no_of_blocks <= 9999

    INTEGER            :: no_of_rows
    INTEGER            :: no_of_cols
    INTEGER, PARAMETER :: row_id_1st = 0
    INTEGER, PARAMETER :: col_id_1st = 0
    INTEGER            :: row_id_last
    INTEGER            :: col_id_last
    INTEGER            :: double_outer_block_id_last

    ! Set up group IDs for inner region and outer region groups of PEs
    INTEGER, PARAMETER :: inner_group_id = 0        ! inner region PEs
    INTEGER, PARAMETER :: outer_group_id = 1        ! outer region PEs
    INTEGER, PARAMETER :: double_outer_group_id = 2 ! two-electron outer region PEs
    INTEGER, PARAMETER :: world_group_id = 3        ! all PEs

    ! group IDs for several MPI tasks per outer region block
    INTEGER            :: first_block_group_id, last_block_group_id  
    
CONTAINS

    SUBROUTINE derive_communication_parameters

        USE initial_conditions, ONLY: no_of_pes_to_use_inner, &
                                      no_of_pes_to_use_outer, &
                                      no_of_pes_to_use_double_outer, &
                                      no_of_pes_per_sector, & 
                                      no_of_pes_to_use

        pe_id_last = no_of_pes_to_use + pe_id_1st - 1
        pe_id_last_inner = no_of_pes_to_use_inner + pe_id_1st_inner - 1
        pe_id_last_outer = no_of_pes_to_use_outer + pe_id_1st_outer - 1
        id_of_1st_pe_outer = pe_id_last_inner + 1
        id_of_last_pe_outer = pe_id_last_inner + pe_id_last_outer + 1

        block_id_last = no_of_pes_to_use_outer /no_of_pes_per_sector - 1
        no_of_blocks = block_id_last - block_id_1st + 1

        !two-electron outer region
        no_of_rows = no_of_pes_to_use_outer
        no_of_cols = no_of_rows
        row_id_last = no_of_rows + row_id_1st - 1
        col_id_last = no_of_cols + col_id_1st - 1
        double_outer_block_id_last = no_of_pes_to_use_double_outer - 1
        pe_id_last_double_outer = no_of_pes_to_use_double_outer + pe_id_1st_double_outer - 1
        id_of_last_pe_double_outer = pe_id_last_outer + pe_id_last_double_outer + 1

    END SUBROUTINE derive_communication_parameters

END MODULE communications_parameters
