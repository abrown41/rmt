! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for inner region parallelisation.

MODULE inner_parallelisation

    IMPLICIT NONE

    PUBLIC setup_inner_region_parallelisation
    PUBLIC deallocate_my_surf_amps
    PUBLIC deallocate_inner_region_parallelisation_2

CONTAINS

    SUBROUTINE setup_inner_region_parallelisation(i_am_inner_master, i_am_in_outer_region, states_per_LML_block, my_ang_mom_id)
!   SUBROUTINE setup_inner_region_parallelisation(i_am_inner_master,i_am_in_outer_region,my_ang_mom_id)

        USE distribute_hd_blocks,  ONLY: setup_mpi_layer_1
        USE distribute_hd_blocks2, ONLY: setup_mpi_layer_2
        USE mpi_layer_lblocks,     ONLY: setup_inner_region_comms
!        use mpi_layer_lblocks,     only: inner_region_rank 

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        LOGICAL, INTENT(IN)    :: i_am_in_outer_region
        INTEGER, INTENT(IN)    :: states_per_LML_block(-1:)
        INTEGER, INTENT(INOUT) :: my_ang_mom_id  ! lowest LML associated with pe

        CALL setup_inner_region_comms(i_am_inner_master, states_per_LML_block, my_ang_mom_id)
!       CALL setup_inner_region_comms(i_am_inner_master,my_ang_mom_id)
!        print *, 'done setup_inner_region_comms, i am', inner_region_rank

        CALL setup_mpi_layer_1(i_am_inner_master, i_am_in_outer_region)
!        print *, 'done mpi_layer_1, i am', inner_region_rank
        CALL setup_mpi_layer_2
!        print *, 'done mpi_layer_2, i am', inner_region_rank

    END SUBROUTINE setup_inner_region_parallelisation

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_my_surf_amps

        USE distribute_hd_blocks, ONLY: dealloc_my_surf_amps

        CALL dealloc_my_surf_amps

    END SUBROUTINE deallocate_my_surf_amps

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_inner_region_parallelisation_2

        USE distribute_hd_blocks2, ONLY: dealloc_distribute_hd_blocks2
        USE mpi_layer_lblocks,     ONLY: dealloc_setup_Lblock_comm_size

        CALL dealloc_setup_Lblock_comm_size
        CALL dealloc_distribute_hd_blocks2

    END SUBROUTINE deallocate_inner_region_parallelisation_2

END MODULE inner_parallelisation
