! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.

!> @ingroup source
!> @brief Module defining the serialisation of derived types representing input data files.
!>
!> # A Brief Introduction To Serialisation
!>
!> Serialisation is the act of taking the in-memory representation of some type, and transforming it into a canonical format that
!> can be used to unambiguously reconstruct an in-memory representation of that type on other machines, by programs compiled with
!> other compilers, and even by other programming languages.
!>
!> The gold standard of serialisation for data-oriented programming languages is Rust's [serde](https://serde.rs), this module
!> follows a similar (albeit simpler) approach.
!>
!> ## Scope
!>
!> This serialisation has been implemented to serve two uses:
!>
!> 1. Allowing F90 structure literals to be created from data files for the purpose of unit testing.
!> 2. Allowing data files to be loaded into other languages for the purposes of analysis or debugging.
!>
!> If other uses become apparent, the serialisation approach may need to be rethought.
!>
!> To accomodate these two use-cases we define the following scope:
!>
!> * We are interested only in serialising data types defined by RMT. This means that we do not need to define a full serialisation
!> framework, and can instead bake RMT derived types straight into the serialisation interface.
!> * We are not interested in deserialisation. RMT will load it's data either using the functions in `hamiltonian_input_file` or by loading F90
!> structure literals.
!> * We are not interested in any kind of configuration parameters. All outputs will be contructed in a pretty-printed style to
!> support potential human checking, but no options will be given to control that style.
!> * We are not interested in defining a fully generic data model. This means that we can make design decisions that are specific to
!> the output formats that we settle on.
!>
!> ## The Data Model
!>
!> This section discusses the abstract data models of Fortran and this module. Details of how this data model then gets serialised
!> are discussed in the decoumentation of `rmt_serialisehd::serialiser` and it's subtypes.
!>
!> ### Fortran's Data Model
!>
!> The Fortran data model consists of:
!>
!> * 5 instrinsic scalar types.
!>   * Integers, of at least 4 kinds.
!>   * Reals, of at least 2 kinds.
!>   * Complex numbers, of at least 2 kinds.
!>   * Logicals, of at least 1 kind.
!>   * Characters, of at least 1 kind.
!> * Derived scalar types containing any number of fields of any type.
!> * N-dimensional arrays of any scalar type where \f(N = [1, \ldots, 15]\f).
!>
!> ### The Serialisation Data Model
!>
!> The serialisation data model consists of:
!>
!> * Characters, representing a string of text. The representation of character types is currently unspecified but is _de facto_
!> ASCII on account of the fact that the only text that we need to serialise is already ASCII. Furthermore only character types
!> containing letters, numbers, and underscores are currently supported due to the fact that other values may not have a simple
!> representation in serialised outputs (though that isn't an issue since we currently only need to serialise identifiers).
!> * Logicals, representing either a true or false state.
!> * 32-bit integers.
!> * 64-bit reals.
!> * Ordered sets of values of any type, referred to as arrays.
!> * N-dimensional arrays of any _scalar_ type (i.e. not an array or an object), referred to as ND arrays.
!> * Anonymous objects representing a set of key-value pairs where the keys are of type character and the values are of any type.
!> * Named objects representing a name and a set of key-value pairs where the name is of type character, the keys are of type
!> character, and the values are of any type.
!>
!> ### Mapping From Fortran's Data Model To The Serialisation Data Model
!>
!> * Fortran character scalar types are mapped to serialisation character types.
!> * Fortran logical types are mapped to serialisation logical types.
!> * Fortran integer types of kinds which are representable by 32-bit integers are mapped to serialisation integer types. Other
!> kinds are currently unsupported.
!> * Fortran real types of kinds which are representable by 64-bit reals are mapped to serialisation real types. Other kinds are
!> currently unsupported.
!> * Fortran complex types are currently unsupported.
!> * Fortran 1-dimensional arrays are mapped either to serialisation arrays or serialisation ND arrays. This decision must be made
!> by the person implementing the mapping from a Fortran data type to a serialisation data type, not the person implementing the
!> serialisation.
!> * Fortran N-dimensional arrays where \f(N = [2, \ldots, 15]\f) are mapped to serialisation ND arrays.
!> * Fortran derived scalar types can be mapped in whichever way the mapper thinks best. Usually they will be mapped to named
!> objects, but in certain situations mapping them to another type (e.g. mapping a linked list to an array) might be beneficial.
!> If a deviation is made then make sure that a method is added to the `assignment(=)` interface to ensure that the F90 serialiser
!> still produces structure literals that can be loaded.
!>
!> ## Module Interface
!>
!> Each output format will provide a `serialise_<output format>` interface which will (statically) dispatch over a series of
!> `serialise_<output format>_<derived type>` functions. These functions will take a derived type scalar and return an allocatable
!> `CHARACTER` scalar. By convention these functions will only be implemented for derived types that are considered to represent
!> data files that RMT must read (e.g. `serialise_f90_H_file` is implemented, but `serialise_f90_L_block` is not). These functions
!> are the only public interface to this module.
!>
!> ## Conventions
!>
!> ### Serialising N-Dimensional Arrays
!>
!> Fortran arrays are are N-dimensional, they have an ordered set of values but they also have a rectangular shape. However most
!> serialisation formats (including F90 source code) have no facilities for defining any array other than an ordered set of values.
!> This is not an issue for serialisation in theory since an N-dimensional array can be expressed as using other primitives, however
!> in _practice_ one of the primitives needed (for a sensible implementation, at least) is an ordered set of values. In Fortran
!> there is no difference between an ordered set of values and a 1-dimensional array, meaning that if we try to naively serialise
!> N-dimensional arrays in a generic way then we will run into infinite recursion when trying to serialise the shape and the values.
!>
!> The are at least two ways to handle this:
!>
!> 1. You could treat all 1-dimensional arrays as ordered sets of values, so when you come to serialise a 1-dimensional you just use
!> the output formats native array style. The benefits of this are that the serialisation data model is an isomorphism of Fortran's
!> data and so any type can be serialised based only on it's type and no other context. The downside of this approach however is
!> that other languages (e.g. Python with NumPy) usually have a distinction between an ordered set of values and a 1-dimensional
!> array so that when _deserialising_ the type from another language you must either forego this distinction or you must make the
!> decision at that point based on the available context.
!> 2. You could create a distinction between N-dimensional arrays and ordered sets of values in the serialisation data model, and
!> require the implementor to decide whether each 1-dimensional arry is conceptually an ordered set of values or not.
!>
!> Option 2 was chosen becaue it requires the decision to be made only at implementation time, instead of each time the data is
!> deserialised. Due to this there are three generic methods that can be used to improve code readability (though they are not
!> necessary, the methods that they dispatch to can be used instead): `serialise`, `serialise_array`, and `serialise_nd_array`.
!> `serialise` is for serialising _scalars_ (of either intrinsic or derived type), `serialise_array` is for serialising ordered sets
!> of values, and `serialise_nd_array` is for serialising N-dimensional arrays. When deciding whether to serialise a 1-dimensional
!> array as an ordered set of values or as an N-dimensional array then a good rule of thumb is to consider whether the array is used
!> as a cohesive unit or if it's elements are always used individually. For example, a vector may be stored as a 1-dimensional array
!> but it is generally used as one cohesive unit so it should be serialised as an N-dimensional array, whereas the list of L blocks
!> in the H file as stored as an array but each L block is treated as a separate unit.
!>
!> ### `start_`, `add_`, `finish_` Methods vs `serialise_` Methods
!>
!> Any method of the serialiser starting with `serialise_` takes the serialiser as an `INTENT(IN)` parameter, meaning that it can't
!> be changed. This was done to make it simpler to serialise compound objects (e.g. derived types or arrays) that are themselves
!> fields of a compound object.But compound objects themselves can't be serialised as a single unit without unacceptable amounts of
!> code duplication. To reduce code duplication, `serialise_<object>` methods for compound objects are defined in terms of
!> `start_<object>`, `add_<object>_field`, and `finish_<object>` methods. These methods take the serialiser as an `INTENT(INOUT)`
!> parameter instead.
!>
!> When writing a new `serialise_` method for a compound object you should allocate a new serialiser using the serialiser passed as
!> the `INTENT(IN)` parameter for the mold. You should then call the appropriate `start_` method exactly once, follwed by the `add_`
!> method as many times as necessary, and finally call the `finish_` methods exactly once. The `finish` method will return a string
!> containing the result of the serialisation.
MODULE rmt_serialiseHD
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE hamiltonian_input_file, ONLY: H_file, L_block
    USE rmt_utilities, ONLY: int_to_char, real_to_es_char

    IMPLICIT NONE

    INTERFACE serialise_json
        MODULE PROCEDURE serialise_json_H_file
    END INTERFACE

    INTERFACE serialise_f90
        MODULE PROCEDURE serialise_f90_H_file
    END INTERFACE

    !> \brief This type defines the mapping from Fortran types to serialisation types, as well as the serialisation interface.
    !>
    !> # Primitives
    !>
    !> This type has a set of methods marked as `DEFERRED`. This is the interface that subtypes need to implement so that they can
    !> serialise the full serialisation data model. Each type in the serialisation data model has either 1, or 3 methods that the
    !> serialiser must implment to serialise that serialisation type. These methods (or group of three methods) are known as
    !> primitives and all other data types are serialised using primitives. A serialiser may choose to implement some primitives
    !> in terms of others, e.g. the JSON serialiser implements ND arrays in terms of the anonymous object primitive.
    !>
    !> Some deferred methods are not part of any primitive (currently `end_line` and `empty_array`), these are added so that typed
    !> arrays can be serialised by a method defined on this serialiser abstract type instead of each concrete serialiser having to
    !> reimplement them.
    !>
    !> # Mapping
    !>
    !> Methods on this type that start with `serialise_` define the mapping from the Fortran data type to the serialisation data
    !> type. Some of the Fortran types map directly to serialisation types, in these cases the primitive is used to do the mapping.
    !> Otherwise a non-overridable method should be created on the abstract type to define the mapping, and this method should be
    !> implemented in terms of other mappings (usually primitives).
    TYPE, ABSTRACT :: serialiser
        PRIVATE

        CHARACTER(len=:), ALLOCATABLE :: buffer
    CONTAINS
        PRIVATE

        PROCEDURE, PUBLIC, NON_OVERRIDABLE :: serialise_H_file

        GENERIC :: serialise => serialise_L_block, &
            serialise_H_file, &
            serialise_character, &
            serialise_integer, &
            serialise_real
        GENERIC :: serialise_array => serialise_array_integer, &
            serialise_array_real, &
            serialise_array_L_block
        GENERIC :: serialise_nd_array => serialise_nd_array_integer, &
            serialise_nd_array_integer_r1, &
            serialise_nd_array_real, &
            serialise_nd_array_real_r1, &
            serialise_nd_array_real_r2, &
            serialise_nd_array_real_r3

        PROCEDURE, NON_OVERRIDABLE :: append, start, finish
        PROCEDURE, NON_OVERRIDABLE :: serialise_L_block
        PROCEDURE, NON_OVERRIDABLE :: serialise_array_integer
        PROCEDURE, NON_OVERRIDABLE :: serialise_array_real
        PROCEDURE, NON_OVERRIDABLE :: serialise_array_L_block
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_integer
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_integer_r1
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_real
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_real_r1
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_real_r2
        PROCEDURE, NON_OVERRIDABLE :: serialise_nd_array_real_r3

        PROCEDURE(end_line), DEFERRED :: end_line
        PROCEDURE(serialise_character), NOPASS, DEFERRED :: serialise_character
        PROCEDURE(serialise_integer), NOPASS, DEFERRED :: serialise_integer
        PROCEDURE(serialise_real), NOPASS, DEFERRED :: serialise_real
        PROCEDURE(start_object), DEFERRED :: start_object
        PROCEDURE(add_object_field), DEFERRED :: add_object_field
        PROCEDURE(finish_object), DEFERRED :: finish_object
        PROCEDURE(serialise_empty_array), NOPASS, DEFERRED :: empty_array
        PROCEDURE(start_array), DEFERRED :: start_array
        PROCEDURE(add_array_field), DEFERRED :: add_array_field
        PROCEDURE(finish_array), DEFERRED :: finish_array
        PROCEDURE(start_nd_array), DEFERRED :: start_nd_array
        PROCEDURE(add_nd_array_field), DEFERRED :: add_nd_array_field
        PROCEDURE(finish_nd_array), DEFERRED :: finish_nd_array
    END TYPE

    ABSTRACT INTERFACE
        SUBROUTINE end_line(self)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
        END SUBROUTINE

        FUNCTION serialise_character(c) RESULT(string)
            CHARACTER(len=*), INTENT(IN) :: c
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        FUNCTION serialise_integer(i) RESULT(string)
            IMPORT INT32

            INTEGER(INT32), INTENT(IN) :: i
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        FUNCTION serialise_real(r) RESULT(string)
            IMPORT REAL64

            REAL(REAL64), INTENT(IN) :: r
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        SUBROUTINE start_object(self, level, name)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=*), INTENT(IN), OPTIONAL :: name
        END SUBROUTINE

        SUBROUTINE add_object_field(self, level, key, VALUE, first)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=*), INTENT(IN) :: key, VALUE
            LOGICAL, INTENT(IN) :: first
        END SUBROUTINE

        FUNCTION finish_object(self, level) RESULT(string)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        FUNCTION serialise_empty_array() RESULT(string)
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        SUBROUTINE start_array(self, level)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
        END SUBROUTINE

        SUBROUTINE add_array_field(self, level, element, first)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=*), INTENT(IN) :: element
            LOGICAL, INTENT(IN) :: first
        END SUBROUTINE

        FUNCTION finish_array(self, level) RESULT(string)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION

        SUBROUTINE start_nd_array(self, level, shape)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            INTEGER, DIMENSION(:), INTENT(IN) :: shape
        END SUBROUTINE

        SUBROUTINE add_nd_array_field(self, level, element, first)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            CHARACTER(len=*), INTENT(IN) :: element
            LOGICAL, INTENT(IN) :: first
        END SUBROUTINE

        FUNCTION finish_nd_array(self, level, shape) RESULT(string)
            IMPORT serialiser

            CLASS(serialiser), INTENT(INOUT) :: self
            INTEGER, INTENT(IN) :: level
            INTEGER, DIMENSION(:), INTENT(IN) :: shape
            CHARACTER(len=:), ALLOCATABLE :: string
        END FUNCTION
    END INTERFACE

    TYPE, EXTENDS(serialiser) :: json_serialiser
    CONTAINS
        PROCEDURE :: end_line => end_line_json
        PROCEDURE, NOPASS :: serialise_character => serialise_character_json
        PROCEDURE, NOPASS :: serialise_integer => serialise_integer_json
        PROCEDURE, NOPASS :: serialise_real => serialise_real_json
        PROCEDURE :: start_object => start_object_json
        PROCEDURE :: add_object_field => add_object_field_json
        PROCEDURE :: finish_object => finish_object_json
        PROCEDURE, NOPASS :: empty_array => empty_array_json
        PROCEDURE :: start_array => start_array_json
        PROCEDURE :: add_array_field => add_array_field_json
        PROCEDURE :: finish_array => finish_array_json
        PROCEDURE :: start_nd_array => start_nd_array_json
        PROCEDURE :: add_nd_array_field => add_nd_array_field_json
        PROCEDURE :: finish_nd_array => finish_nd_array_json
    END TYPE

    TYPE, EXTENDS(serialiser) :: f90_serialiser
    CONTAINS
        PROCEDURE :: end_line => end_line_f90
        PROCEDURE, NOPASS :: serialise_character => serialise_character_f90
        PROCEDURE, NOPASS :: serialise_integer => serialise_integer_f90
        PROCEDURE, NOPASS :: serialise_real => serialise_real_f90
        PROCEDURE :: start_object => start_object_f90
        PROCEDURE :: add_object_field => add_object_field_f90
        PROCEDURE :: finish_object => finish_object_f90
        PROCEDURE, NOPASS :: empty_array => empty_array_f90
        PROCEDURE :: start_array => start_array_f90
        PROCEDURE :: add_array_field => add_array_field_f90
        PROCEDURE :: finish_array => finish_array_f90
        PROCEDURE :: start_nd_array => start_nd_array_f90
        PROCEDURE :: add_nd_array_field => add_nd_array_field_f90
        PROCEDURE :: finish_nd_array => finish_nd_array_f90
    END TYPE

    CHARACTER(len=*), PARAMETER :: nl = NEW_LINE('A'), indent = "    "

    PRIVATE

    PUBLIC serialise_f90, serialise_json
CONTAINS
    FUNCTION serialise_json_H_file(H) RESULT(string)
        TYPE(H_file), INTENT(IN) :: H
        CHARACTER(len=:), ALLOCATABLE :: string

        TYPE(json_serialiser) :: ser

        string = ser%serialise_H_file(0, H)
    END FUNCTION

    FUNCTION serialise_f90_H_file(H) RESULT(string)
        TYPE(H_file), INTENT(IN) :: H
        CHARACTER(len=:), ALLOCATABLE :: string

        TYPE(f90_serialiser) :: ser

        string = ser%serialise_H_file(0, H)
    END FUNCTION

    SUBROUTINE append(self, string)
        CLASS(serialiser), INTENT(INOUT) :: self
        CHARACTER(len=*), INTENT(IN) :: string

        self%buffer = self%buffer//string
    END SUBROUTINE

    SUBROUTINE start(self)
        CLASS(serialiser), INTENT(INOUT) :: self

        IF (ALLOCATED(self%buffer)) THEN
            ERROR STOP "Serialiser is already being used, please create a new one for this serialisation."
        END IF

        self%buffer = ""
    END SUBROUTINE

    FUNCTION finish(self) RESULT(string)
        CLASS(serialiser), INTENT(INOUT) :: self
        CHARACTER(len=:), ALLOCATABLE :: string

        IF (.NOT. ALLOCATED(self%buffer)) THEN
            ERROR STOP "Serialiser has not been filled, did you remember to call `start`?"
        END IF

        string = self%buffer
    END FUNCTION

    FUNCTION serialise_L_block(self, level, L) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        TYPE(L_block), INTENT(IN) :: L
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: s

        ALLOCATE (s, mold=self)

        CALL s%start_object(level, "L_block")

        CALL s%add_object_field(level, "lrgl", self%serialise(L%lrgl), .TRUE.)
        CALL s%add_object_field(level, "nspn", self%serialise(L%nspn), .FALSE.)
        CALL s%add_object_field(level, "npty", self%serialise(L%npty), .FALSE.)
        CALL s%add_object_field(level, "nchan", self%serialise(L%nchan), .FALSE.)
        CALL s%add_object_field(level, "nstat", self%serialise(L%nstat), .FALSE.)

        ! TODO: Trying to use the generics on these leads to an ICE in GFortran...
        CALL s%add_object_field(level, "nconat", self%serialise_nd_array_integer_r1(level + 1, L%nconat), .FALSE.)
        CALL s%add_object_field(level, "l2p", self%serialise_nd_array_integer_r1(level + 1, L%l2p), .FALSE.)
        CALL s%add_object_field(level, "cf", self%serialise_nd_array_real_r3(level + 1, L%cf), .FALSE.)
        CALL s%add_object_field(level, "eig", self%serialise_nd_array_real_r1(level + 1, L%eig), .FALSE.)
        CALL s%add_object_field(level, "wmat", self%serialise_nd_array_real_r2(level + 1, L%wmat), .FALSE.)

        string = s%finish_object(level)

        DEALLOCATE (s)
    END FUNCTION

    FUNCTION serialise_H_file(self, level, H) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        TYPE(H_file), INTENT(IN) :: H
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: s

        ALLOCATE (s, mold=self)

        CALL s%start_object(level, "H_file")

        CALL s%add_object_field(level, "nelc", self%serialise(H%nelc), .TRUE.)
        CALL s%add_object_field(level, "nz", self%serialise(H%nz), .FALSE.)
        CALL s%add_object_field(level, "lrang2", self%serialise(H%lrang2), .FALSE.)
        CALL s%add_object_field(level, "lamax", self%serialise(H%lamax), .FALSE.)
        CALL s%add_object_field(level, "ntarg", self%serialise(H%ntarg), .FALSE.)
        CALL s%add_object_field(level, "rmatr", self%serialise(H%rmatr), .FALSE.)
        CALL s%add_object_field(level, "bbloch", self%serialise(H%bbloch), .FALSE.)

        ! TODO: Trying to use the generics on these leads to an ICE in GFortran.., .FALSE..
        CALL s%add_object_field(level, "etarg", self%serialise_nd_array_real_r1(level + 1, H%etarg), .FALSE.)
        CALL s%add_object_field(level, "ltarg", self%serialise_nd_array_integer_r1(level + 1, H%ltarg), .FALSE.)
        CALL s%add_object_field(level, "starg", self%serialise_nd_array_integer_r1(level + 1, H%starg), .FALSE.)
        CALL s%add_object_field(level, "cfbut", self%serialise_nd_array_real_r2(level + 1, H%cfbut), .FALSE.)

        CALL s%add_object_field(level, "L_blocks", self%serialise_array_L_block(level + 1, H%L_blocks), .FALSE.)

        string = s%finish_object(level)

        DEALLOCATE (s)
    END FUNCTION

    FUNCTION serialise_array_integer(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER(INT32), DIMENSION(:), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: s
        INTEGER :: i

        IF (SIZE(array) == 0) THEN
            string = self%empty_array()
            RETURN
        END IF

        ALLOCATE (s, mold=self)

        CALL s%start_array(level)
        DO i = 1, SIZE(array)
            CALL s%add_array_field(level, s%serialise(array(i)), i == 1)
        END DO
        string = s%finish_array(level)

        DEALLOCATE (s)
    END FUNCTION

    FUNCTION serialise_array_real(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        REAL(REAL64), DIMENSION(:), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: s
        INTEGER :: i

        IF (SIZE(array) == 0) THEN
            string = self%empty_array()
            RETURN
        END IF

        ALLOCATE (s, mold=self)

        CALL s%start_array(level)
        DO i = 1, SIZE(array)
            CALL s%add_array_field(level, s%serialise(array(i)), i == 1)
        END DO
        string = s%finish_array(level)

        DEALLOCATE (s)
    END FUNCTION

    FUNCTION serialise_array_L_block(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        TYPE(L_block), DIMENSION(:), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: s
        INTEGER :: i

        IF (SIZE(array) == 0) THEN
            string = self%empty_array()
            RETURN
        END IF

        ALLOCATE (s, mold=self)

        CALL s%start_array(level)
        DO i = 1, SIZE(array)
            CALL s%add_array_field(level, s%serialise_L_block(level + 1, array(i)), i == 1)
        END DO
        string = s%finish_array(level)

        DEALLOCATE (s)
    END FUNCTION

    FUNCTION serialise_nd_array_integer(self, level, shape, values) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape
        INTEGER(INT32), DIMENSION(:), INTENT(IN) :: values
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: ser
        INTEGER :: i

        ALLOCATE (ser, mold=self)

        CALL ser%start_nd_array(level, shape)
        DO i = 1, SIZE(values)
            CALL ser%add_nd_array_field(level, ser%serialise(values(i)), i == 1)
        END DO
        string = ser%finish_nd_array(level, shape)

        DEALLOCATE (ser)
    END FUNCTION

    FUNCTION serialise_nd_array_integer_r1(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER(INT32), DIMENSION(:), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        string = self%serialise_nd_array_integer(level, SHAPE(array), RESHAPE(array, [SIZE(array)]))
    END FUNCTION

    FUNCTION serialise_nd_array_real(self, level, shape, values) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape
        REAL(REAL64), DIMENSION(:), INTENT(IN) :: values
        CHARACTER(len=:), ALLOCATABLE :: string

        CLASS(serialiser), ALLOCATABLE :: ser
        INTEGER :: i

        ALLOCATE (ser, mold=self)

        CALL ser%start_nd_array(level, shape)
        DO i = 1, SIZE(values)
            CALL ser%add_nd_array_field(level, ser%serialise(values(i)), i == 1)
        END DO
        string = ser%finish_nd_array(level, shape)

        DEALLOCATE (ser)
    END FUNCTION

    FUNCTION serialise_nd_array_real_r1(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        REAL(REAL64), DIMENSION(:), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        string = self%serialise_nd_array_real(level, SHAPE(array), RESHAPE(array, [SIZE(array)]))
    END FUNCTION

    FUNCTION serialise_nd_array_real_r2(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        REAL(REAL64), DIMENSION(:, :), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        string = self%serialise_nd_array_real(level, SHAPE(array), RESHAPE(array, [SIZE(array)]))
    END FUNCTION

    FUNCTION serialise_nd_array_real_r3(self, level, array) RESULT(string)
        CLASS(serialiser), INTENT(IN) :: self
        INTEGER, INTENT(IN) :: level
        REAL(REAL64), DIMENSION(:, :, :), INTENT(IN) :: array
        CHARACTER(len=:), ALLOCATABLE :: string

        string = self%serialise_nd_array_real(level, SHAPE(array), RESHAPE(array, [SIZE(array)]))
    END FUNCTION

    SUBROUTINE end_line_json(self)
        CLASS(json_serialiser), INTENT(inout) :: self

        CALL self%append(nl)
    END SUBROUTINE end_line_json

    FUNCTION serialise_character_json(c) RESULT(string)
        CHARACTER(len=*), INTENT(IN) :: c
        CHARACTER(len=:), ALLOCATABLE :: string

        INTEGER :: i
        CHARACTER(len=1) :: x

        DO i = 1, LEN(c)
            x = c(i:i)
            IF (.NOT. ( &
                (x >= 'a' .AND. x <= 'z') .OR. &
                (x >= 'A' .AND. x <= 'Z') .OR. &
                (x >= '0' .AND. x <= '9') .OR. &
                x == '_' &
                )) THEN
                ERROR STOP "Can only serialise letters, numbers, and underscores."
            END IF
        END DO

        string = '"'//c//'"'
    END FUNCTION

    FUNCTION serialise_integer_json(i) RESULT(string)
        INTEGER(INT32), INTENT(IN) :: i
        CHARACTER(len=:), ALLOCATABLE :: string

        string = int_to_char(i)
    END FUNCTION

    FUNCTION serialise_real_json(r) RESULT(string)
        REAL(REAL64), INTENT(IN) :: r
        CHARACTER(len=:), ALLOCATABLE :: string

        string = real_to_es_char(r)
    END FUNCTION

    SUBROUTINE start_object_json(self, level, name)
        CLASS(JSON_serialiser), INTENT(inout) :: self
        INTEGER, INTENT(in) :: level
        CHARACTER(len=*), INTENT(IN), OPTIONAL :: name

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (name => name, level => level); END ASSOCIATE

        CALL self%start()
        CALL self%append("{")
    END SUBROUTINE start_object_json

    SUBROUTINE add_object_field_json(self, level, key, VALUE, first)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: key, VALUE
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append(self%serialise_character(key))
        CALL self%append(': ')
        CALL self%append(VALUE)
    END SUBROUTINE

    FUNCTION finish_object_json(self, level) RESULT(string)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=:), ALLOCATABLE :: string

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level))
        CALL self%append("}")

        string = self%finish()
    END FUNCTION

    FUNCTION empty_array_json() RESULT(string)
        CHARACTER(len=:), ALLOCATABLE :: string

        string = "[]"
    END FUNCTION empty_array_json

    SUBROUTINE start_array_json(self, level)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (level => level); END ASSOCIATE

        CALL self%start()
        CALL self%append("[")
    END SUBROUTINE

    SUBROUTINE add_array_field_json(self, level, element, first)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: element
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append(element)
    END SUBROUTINE

    FUNCTION finish_array_json(self, level) RESULT(string)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=:), ALLOCATABLE :: string

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level))
        CALL self%append("]")

        string = self%finish()
    END FUNCTION

    SUBROUTINE start_nd_array_json(self, level, shape)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape

        CALL self%start_object(level)
        CALL self%add_object_field(level, "format", self%serialise("fortran"), .TRUE.)
        ! TODO: Trying to use the generics on this leads to an ICE in GFortran...
        CALL self%add_object_field(level, "shape", self%serialise_array_integer(level + 1, shape), .FALSE.)
        ! TODO: This is abuse...
        CALL self%add_object_field(level, "values", "[", .FALSE.)
    END SUBROUTINE

    SUBROUTINE add_nd_array_field_json(self, level, element, first)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: element
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 2))
        CALL self%append(element)
    END SUBROUTINE

    FUNCTION finish_nd_array_json(self, level, shape) RESULT(string)
        CLASS(json_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape
        CHARACTER(len=:), ALLOCATABLE :: string

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (shape => shape); END ASSOCIATE

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append("]")

        string = self%finish_object(level)
    END FUNCTION

    SUBROUTINE end_line_f90(self)
        CLASS(f90_serialiser), INTENT(inout) :: self

        CALL self%append(" &")
        CALL self%append(nl)
    END SUBROUTINE end_line_f90

    FUNCTION serialise_character_f90(c) RESULT(string)
        CHARACTER(len=*), INTENT(IN) :: c
        CHARACTER(len=:), ALLOCATABLE :: string

        string = '"'//c//'"'
    END FUNCTION

    FUNCTION serialise_integer_f90(i) RESULT(string)
        INTEGER(INT32), INTENT(IN) :: i
        CHARACTER(len=:), ALLOCATABLE :: string

        string = int_to_char(i)
    END FUNCTION

    FUNCTION serialise_real_f90(r) RESULT(string)
        REAL(REAL64), INTENT(IN) :: r
        CHARACTER(len=:), ALLOCATABLE :: string

        string = real_to_es_char(r)
    END FUNCTION

    SUBROUTINE start_object_f90(self, level, name)
        CLASS(f90_serialiser), INTENT(inout) :: self
        INTEGER, INTENT(in) :: level
        CHARACTER(len=*), INTENT(IN), OPTIONAL :: name

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (level => level); END ASSOCIATE

        ! TODO: Ideally we'd have a difference between objects and derived types, but it doesn't really seem worth it...
        IF (.NOT. PRESENT(name)) THEN
            ERROR STOP "Name must be provided for the F90 serialiser."
        END IF

        CALL self%start()
        CALL self%append(name)
        CALL self%append("(")
    END SUBROUTINE start_object_f90

    SUBROUTINE add_object_field_f90(self, level, key, VALUE, first)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: key, VALUE
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append(key)
        CALL self%append(' = ')
        CALL self%append(VALUE)
    END SUBROUTINE

    FUNCTION finish_object_f90(self, level) RESULT(string)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=:), ALLOCATABLE :: string

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level))
        CALL self%append(")")

        string = self%finish()
    END FUNCTION

    FUNCTION empty_array_f90() RESULT(string)
        CHARACTER(len=:), ALLOCATABLE :: string

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (string => string); END ASSOCIATE

        ERROR STOP "No reliable way to serialise an empty array in F90."
    END FUNCTION empty_array_f90

    SUBROUTINE start_array_f90(self, level)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (level => level); END ASSOCIATE

        CALL self%start()
        CALL self%append("[")
    END SUBROUTINE

    SUBROUTINE add_array_field_f90(self, level, element, first)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: element
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append(element)
    END SUBROUTINE

    FUNCTION finish_array_f90(self, level) RESULT(string)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=:), ALLOCATABLE :: string

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level))
        CALL self%append("]")

        string = self%finish()
    END FUNCTION

    SUBROUTINE start_nd_array_f90(self, level, shape)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape

        ! Suppress unused-dummy-argument warning.
        ASSOCIATE (shape => shape); END ASSOCIATE

        CALL self%start()
        CALL self%append("RESHAPE(")
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append("[")
    END SUBROUTINE

    SUBROUTINE add_nd_array_field_f90(self, level, element, first)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        CHARACTER(len=*), INTENT(IN) :: element
        LOGICAL, INTENT(IN) :: first

        IF (.NOT. first) THEN
            CALL self%append(",")
        END IF
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 2))
        CALL self%append(element)
    END SUBROUTINE

    FUNCTION finish_nd_array_f90(self, level, shape) RESULT(string)
        CLASS(f90_serialiser), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: level
        INTEGER, DIMENSION(:), INTENT(IN) :: shape
        CHARACTER(len=:), ALLOCATABLE :: string

        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append("],")
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level + 1))
        CALL self%append("shape = ")
        ! TODO: Trying to use the generics on this leads to an ICE in GFortran...
        CALL self%append(self%serialise_array_integer(level + 1, shape))
        CALL self%end_line()
        CALL self%append(REPEAT(indent, level))
        CALL self%append(")")

        string = self%finish()
    END FUNCTION
END MODULE rmt_serialiseHD
