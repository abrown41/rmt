! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> \authors J Benda
!> \date    2018
!>
!> \brief Handles all dipole (and other) coupling rules that are used in other modules,
!> most prominently in \ref outer_hamiltonian.

MODULE coupling_rules

    USE precisn, ONLY: wp

    IMPLICIT NONE

CONTAINS

    !> \brief   Check if two channels are coupled by dipole operator (target)
    !> \authors J Benda
    !> \date    2018
    !>
    !> Determines whether the ionized target wave functions are coupled by the dipole operator.
    !> This is used before applying the outer potential \f$ W_D \f$.
    !>
    !> \note Presently, it is assumed that any pair of channels is coupled by \f$ W_D \f$
    !>       in molecular mode.
    !>
    PURE LOGICAL FUNCTION target_coupled(i, j) RESULT (verdict)

        USE initial_conditions, ONLY: molecular_target, &
                                      LS_coupling_id,   &
                                      coupling_id
        USE lrpots,             ONLY: lpuu, ltuu, lmuu

        INTEGER, INTENT(IN) :: i, j
        INTEGER             :: Li, Lj, Pi, Pj, MLi, MLj

        Li = ltuu(i); Pi = lpuu(i); MLi = lmuu(i)
        Lj = ltuu(j); Pj = lpuu(j); MLj = lmuu(j)

        IF (molecular_target) THEN
            verdict = .TRUE.  ! assume worst case for now
        ELSE
            IF (coupling_id.EQ.LS_coupling_id) THEN
                verdict = (ABS(Li - Lj) <= 1 .AND. Pi /= Pj .AND. ABS(MLi - MLj) <= 1)
            ELSE
                verdict = (ABS(Li - Lj) <= 2 .AND. Pi /= Pj .AND. ABS(MLi - MLj) <= 2)
            END IF
        END IF

    END FUNCTION target_coupled


    !> \brief   Check if two channels are coupled by dipole operator (projectile, length gauge)
    !> \authors J Benda
    !> \date    2018
    !>
    !> Determines whether the ionized electron wave functions in different channes are coupled
    !> by the dipole operator in the length gauge. This is used before applying the outer
    !> potential \f$ W_P \f$.
    !>
    !> \note Presently, it is assumed that any pair of channels is coupled by \f$ W_P \f$
    !>       in molecular mode.
    !>
    PURE LOGICAL FUNCTION projectile_lcoupled(i, j) RESULT (verdict)

        USE initial_conditions, ONLY: molecular_target, &
                                      LS_coupling_id,   &
                                      coupling_id
        USE lrpots,             ONLY: liuu, lpuu, ltuu, lmuu

        INTEGER, INTENT(IN) :: i, j
        INTEGER             :: Li, Lj, Pi, Pj, MLi, MLj, elli, ellj

        Li = ltuu(i); Pi = lpuu(i); MLi = lmuu(i); elli = liuu(i)
        Lj = ltuu(j); Pj = lpuu(j); MLj = lmuu(j); ellj = liuu(j)

        IF (molecular_target) THEN
            verdict = .TRUE.  ! assume worst case for now
        ELSE
            IF (coupling_id.EQ.LS_coupling_id) THEN
                verdict = (ABS(Lj - Li) <= 1 .AND. Pi /= Pj .AND. ABS(MLj - MLi) <= 1 .AND. ABS(ellj - elli) == 1)
            ELSE
                verdict = (ABS(Lj - Li) <= 2 .AND. Pi /= Pj .AND. ABS(MLj - MLi) <= 2 .AND. ABS(ellj - elli) == 1)
            END IF
        END IF

    END FUNCTION projectile_lcoupled


    !> \brief   Check if two channels are coupled by dipole operator (projectile, velocity gauge)
    !> \authors J Benda
    !> \date    2018
    !>
    !> Determines whether the ionized electron wave functions in different channels are coupled
    !> by the dipole operator in the velocity gauge. This is used before applying the outer
    !> potential \f$ W_P \f$.
    !>
    !> \note Presently, this function does not support molecular mode.
    !>
    PURE LOGICAL FUNCTION projectile_vcoupled(i, j) RESULT (verdict)

        USE initial_conditions, ONLY: molecular_target, &
                                      LS_coupling_id,   &
                                      coupling_id
        USE lrpots,             ONLY: lpuu, ltuu, lmuu

        INTEGER, INTENT(IN) :: i, j
        INTEGER             :: Li, Lj, Pi, Pj, MLi, MLj

        Li = ltuu(i); Pi = lpuu(i); MLi = lmuu(i)
        Lj = ltuu(j); Pj = lpuu(j); MLj = lmuu(j)

        IF (molecular_target) THEN
            verdict = .FALSE.  ! not supported at all in molecular mode
        ELSE
            IF (coupling_id.EQ.LS_coupling_id) THEN
                verdict = (ABS(Lj - Li) <= 1 .AND. Pi /= Pj .AND. ABS(MLj - MLi) <= 1)
            ELSE
                verdict = (ABS(Lj - Li) <= 2 .AND. Pi /= Pj .AND. ABS(MLj - MLi) <= 2)
            END IF
        END IF

    END FUNCTION projectile_vcoupled

    !> \brief   Calculates a dot product of the electric field with another vector
    !> \authors J Benda
    !> \date    2018
    !>
    !> In molecular case, this function will ignore parameters Mj and Mi and calculate a simple
    !> dot product of the field vector and the other vector.
    !>
    !> In the atomic case, though, it is assumed that the other vector is passed to this function in spherical
    !> basis. The field vector is then converted also to the spherical basis and only the element corresponding
    !> to the supplied magnetic quantum numbers is returned.
    !>
    PURE COMPLEX(wp) FUNCTION field_contraction(field_vector, other_vector, Mj, Mi) RESULT (output)

        USE electric_field,     ONLY: field_sph_component
        USE initial_conditions, ONLY: molecular_target, &
                                      LS_coupling_id,   &
                                      coupling_id

        REAL(wp),    INTENT(IN) :: field_vector(3)
        COMPLEX(wp), INTENT(IN) :: other_vector(3)
        INTEGER,     INTENT(IN) :: Mj, Mi

        INTEGER :: delta_ML

        IF (molecular_target) THEN
            output = dot_product(field_vector, other_vector)
        ELSE
           IF (coupling_id.EQ.LS_coupling_id) THEN
              delta_ML = Mj - Mi
           ELSE
              delta_ML = (Mj-Mi)/2
           END IF
            output = field_sph_component(field_vector, delta_ML) * other_vector(2 - delta_ML)
        END IF

    END FUNCTION field_contraction


    !> \brief   Convert field components to factors used in inner region
    !> \authors J Benda
    !> \date    2018
    !>
    !> Given the Cartesian components of the electric field, computes factors for down/same/up dipole blocks.
    !> In molecular case, this is just a trivial copy: each of the three dipole blocks has to be multiplied by the
    !> corresponding Cartesian component of the field. However, in the atomic case the dipole blocks have to be
    !> multiplied by the Clebsch-Gordan coefficient corresponding to the transition, and not by a Cartesian component
    !> but a spherical component. The choice of the spherical component is governed by the difference between
    !> the coupled blocks' magnetic numbers.
    !>
    SUBROUTINE field_factors(field_vector, j, i, field_factor)

        USE electric_field,     ONLY: field_sph_component
        USE initial_conditions, ONLY: molecular_target, &
                                      LS_coupling_id,   &
                                      coupling_id
        USE readhd,             ONLY: LML_block_lrgl, LML_block_ml, LML_block_npty, cg_store

        REAL(wp),    INTENT(IN)  :: field_vector(3)
        COMPLEX(wp), INTENT(OUT) :: field_factor(3)
        INTEGER,     INTENT(IN)  :: j, i

        REAL(wp)    :: sign_factor
        INTEGER     :: delta_ML
        COMPLEX(wp) :: field_comp

        IF (molecular_target) THEN
            field_factor(:) = field_vector(:)
        ELSEIF (coupling_id.eq.LS_coupling_id) then
            delta_ML = LML_block_ml(j) - LML_block_ml(i)
            IF (ABS(delta_ML) <= 1) THEN
                field_comp = field_sph_component(field_vector,  -delta_ML)
                sign_factor = MERGE((-1.0_wp)**(LML_block_lrgl(i) - LML_block_lrgl(j) + 1), 1.0_wp, LML_block_npty(i) == 0)
                field_factor(:) = sign_factor * field_comp * cg_store(i,j)
            ELSE
                field_factor(:) = 0
            END IF
        ELSE
            delta_ML = LML_block_ml(j) - LML_block_ml(i)
            IF (ABS(delta_ML) <= 2) THEN
                field_comp = field_sph_component(field_vector, -delta_ML/2)
                sign_factor = MERGE((-1.0_wp)**((LML_block_lrgl(i) - LML_block_lrgl(j))/2 + 1), 1.0_wp, LML_block_npty(i) == 0)
                field_factor(:) = sign_factor * field_comp * cg_store(i,j)
            ELSE
                field_factor(:) = 0
            END IF
        END IF

    END SUBROUTINE field_factors

END MODULE coupling_rules
