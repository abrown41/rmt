! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Routines for handling outer region Hamiltonian-wavefunction multiplication.
!> Including incrementing with the second derivative and Laplacian, 
!> applying the absorbing boundary, and calculating the ionizated population. 
!> Incrementing with the long-range potential matrices, is handled in @ref outer_hamiltonian 
!> and @ref outer_hamiltonian_atrlessthanb.

MODULE local_ham_matrix

    USE precisn,                ONLY: wp
    USE rmt_assert,             ONLY: assert
    USE calculation_parameters, ONLY: Lmax_At_bndry, &
                                      R_cutoff, &
                                      half_fd_order
    USE grid_parameters,        ONLY: x_last, &
                                      x_1st, &
                                      R_1st, &
                                      R_last
    USE initial_conditions,     ONLY: deltaR,&
                                      offset => offset_in_stepsizes, &
                                      window_harmonics,&
                                      window_cutoff,&
                                      window_FWHM

    IMPLICIT NONE

    PUBLIC

    ! Absorbing pot
!   REAL(wp), PRIVATE,SAVE  ::  absorb    (x_1st:x_Last)
!   REAL(wp), PRIVATE,SAVE  ::  absorb_sq (x_1st:x_Last)
   
!   REAL(wp),SAVE           ::  RR        (x_1st:x_last)
!   REAL(wp),SAVE           ::  Rinverse  (x_1st:x_last)
!   REAL(wp),SAVE           ::  Rinverse2 (x_1st:x_last)
!   REAL(wp),SAVE           ::  one_over_delR
!   INTEGER,SAVE            ::  R_id_at   (x_1st:x_last)

    REAL(wp), PRIVATE, SAVE, ALLOCATABLE  ::  absorb(:)
    REAL(wp), PRIVATE, SAVE, ALLOCATABLE  ::  absorb_sq(:)
 
    REAL(wp), SAVE, ALLOCATABLE           ::  master_RR(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  windowed_RR(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  RR(:)
    Real(wp), SAVE                        ::  r_value_at_r1st
    REAL(wp), SAVE, ALLOCATABLE           ::  Rinverse(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  Rinverse2(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  r_greater(:, :)
    REAL(wp), SAVE, ALLOCATABLE           ::  r_less_over_r_greater(:, :)
    REAL(wp), SAVE                        ::  one_over_delR
    REAL(wp), SAVE, ALLOCATABLE           ::  Rweight(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  quad_and_fd_coeffs(:, :)
    INTEGER, SAVE, ALLOCATABLE            ::  R_id_at(:)


CONTAINS
 
    SUBROUTINE init_local_ham_matrix_module(r_at_region_bndry, delta_r_value, my_r_start_id, &
                                            i_am_outer_master, i_am_in_first_outer_block, i_am_in_last_outer_block)

        USE calculation_parameters,    ONLY: single_ioniz_bndry_in_au, &
                                             single_ioniz_bndry_in_grid_pts,&
                                             nfdm,&
                                             half_fd_order
        USE communications_parameters, ONLY: id_of_last_pe_outer
        USE grid_parameters,           ONLY: channel_id_1st, &
                                             channel_id_last, &
                                             my_channel_id_1st, my_channel_id_last, my_num_channels
        USE mpi_communications,        ONLY: get_my_pe_id
 
        REAL(wp), INTENT(IN)   :: r_at_region_bndry
        REAL(wp), INTENT(IN)   :: delta_r_value
        INTEGER, INTENT(IN)    :: my_r_start_id
        LOGICAL, INTENT(IN)    :: i_am_outer_master, i_am_in_first_outer_block, i_am_in_last_outer_block
        INTEGER                :: i, j, err
        INTEGER                :: my_pe_id, ch_id 
        INTEGER :: max_points_for_H_op

        max_points_for_H_op = 2 * nfdm - half_fd_order

        ! ALLOCATIONS
        ! Hugo: NOTE x_1st assumed to equal 1
        ALLOCATE (absorb(x_1st:x_last), absorb_sq(x_1st:x_last), &
                  master_RR(max_points_for_H_op), &
                  windowed_RR(x_1st:x_last),&
                  RR(x_1st:x_last), Rinverse(x_1st:x_last), &
                  Rinverse2(x_1st:x_last), R_id_at(x_1st:x_last), &
                  r_greater(x_1st:x_last, x_1st:x_last), &
                  r_less_over_r_greater(x_1st:x_last, x_1st:x_last), &
                  Rweight(x_1st:x_last*(my_channel_id_last - my_channel_id_1st + 1)), & 
                  quad_and_fd_coeffs(2*half_fd_order + 1, 2), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in init_local_ham_matrix')

        ! INITIALIZE CONSTANT ARRAYS
        define_r_id: DO i = x_1st, x_last
            R_id_at(i) = my_r_start_id - R_1st + 1 + i - x_1st
        END DO define_r_ID

        define_grid: DO i = x_1st, x_last
            IF (r_at_region_bndry .GT. 0) THEN
                RR(i) = r_at_region_bndry + deltaR*(R_id_at(i) - 1 + offset)  ! RR(x_1st) = r_at_region_bndry + offset*deltaR on 1st PE in outer region
            ELSE
                RR(i) = deltaR*(R_id_at(i) + offset)
            END IF
        END DO define_grid

        r_value_at_r1st = RR(x_1st)

        DO i = 1, max_points_for_H_op
            master_RR(i) = r_value_at_r1st + deltaR * (REAL(-nfdm+i-1,wp))
        END DO

        DO i = x_1st, x_last
            IF ((RR(i) > window_cutoff) .AND. (window_harmonics)) THEN
                windowed_RR(i) = RR(i) * exp(-(window_cutoff - RR(i))*(window_cutoff - RR(i))/(window_FWHM*window_FWHM))
            ELSE
                windowed_RR(i) = RR(i)
            END IF
        END DO


        DO i = x_1st, x_last
            Rinverse(i) = 1.0_wp/RR(i)
        END DO

        DO i = x_1st, x_last
            Rinverse2(i) = 1.0_wp/(RR(i)*RR(i))
        END DO

        DO i = x_1st, x_last
            DO j = x_1st, x_last
                r_greater(i, j) = MERGE(RR(j), RR(i), (i >= j))
                r_less_over_r_greater(i, j) = MERGE(RR(i)/RR(j), RR(j)/RR(i), (i >= j))
            END DO
        END DO

        CALL get_my_pe_id(my_pe_id)

        ! Change - the last grid point is 1 beyond the end point of the last grid element
        ! where the value is set to 0. Thus slightly amend the grid: all sectors should
        ! be a multiple of 4.

! Note by MP: the following commented out section was in the pre-channel-parallelization code in commented
! out form and would need modifying (channel_id_... -> my_channel_id_... etc if it is uncommented
!       IF (i_am_outer_master) THEN
!           DO ch_id=channel_id_1st, channel_id_last
!               rweight(x_1st+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 28._wp/90._wp
!               DO i = x_1st+1, x_last-3, 4
!                   rweight(i+(ch_id-channel_id_1st)*(x_last-x_1st+1))   = 128._wp/90._wp
!                   rweight(i+1+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 48._wp/90._wp
!                   rweight(i+2+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 128._wp/90._wp
!                   rweight(i+3+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 56._wp/90._wp
!               END DO
!               IF (my_pe_id .EQ. pe_id_last) THEN
!                   rweight(x_last+(ch_id-channel_id_1st)*(x_last-x_1st+1))=28._wp/90._wp
!               END IF
!           END DO
!       ELSE
!           DO ch_id=channel_id_1st, channel_id_last
!               DO i = x_1st, x_last-3, 4
!                   rweight(i+(ch_id-channel_id_1st)*(x_last-x_1st+1))   = 128._wp/90._wp
!                   rweight(i+1+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 48._wp/90._wp
!                   rweight(i+2+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 128._wp/90._wp
!                   rweight(i+3+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 56._wp/90._wp
!               END DO
!               IF (my_pe_id .EQ. pe_id_last) THEN
!                   rweight(x_last+(ch_id-channel_id_1st)*(x_last-x_1st+1))=28._wp/90._wp
!               END IF
!           END DO
!       END IF

        CALL calculate_quadrature_and_1st_deriv_coeffs(offset, quad_and_fd_coeffs)

        DO ch_id = my_channel_id_1st, my_channel_id_last
            DO i = x_1st, x_last - 3, 4
                rweight(i + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = 56._wp/90._wp
                rweight(i + 1 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = 128._wp/90._wp
                rweight(i + 2 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = 48._wp/90._wp
                rweight(i + 3 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = 128._wp/90._wp
            END DO
            IF (i_am_in_first_outer_block) THEN
                rweight(x_1st + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = 28._wp/90._wp
                ! If an offset outer grid is to be used, calculate quadrature weights for the first outer sector
                IF (offset > 0.0_wp) THEN
                    DO j = 1, 5
                        rweight(x_1st + j - 1 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = quad_and_fd_coeffs(j, 1)
                    END DO
                    ! Add overlap with first point in second outer sector
                    rweight(x_1st + 4 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = &
                    rweight(x_1st + 4 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) + 28._wp/90._wp
                END IF
            END IF
            ! If an offset outer grid is to be used, calculate quadrature weights for the last outer sector
            IF (offset > 0.0_wp) THEN
                IF (i_am_in_last_outer_block) THEN
                    DO j = 1, 5
                        rweight(x_last - j + 1 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = quad_and_fd_coeffs(j, 1)
                    END DO
                    ! Add overlap with last point in penultimate sector
                    rweight(x_last - 4 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) = &
                    rweight(x_last - 4 + (ch_id - my_channel_id_1st)*(x_last - x_1st + 1)) + 28._wp/90._wp
                END IF
            END IF
        END DO

        one_over_delR = 1.0_wp/delta_r_value

        ! Find the value (in grid points) of the boundary beyond which we have single ionization
        single_ioniz_bndry_in_grid_pts = NINT(single_ioniz_bndry_in_au/deltaR)
        single_ioniz_bndry_in_grid_pts = MAX(R_1st, single_ioniz_bndry_in_grid_pts)
        single_ioniz_bndry_in_grid_pts = MIN(R_Last, single_ioniz_bndry_in_grid_pts)

        ! Initialize the Absorb array

        CALL init_absorb

        CALL test_derivatives_of_r(RR, delta_r_value)

    END SUBROUTINE init_local_ham_matrix_module

!-------------------------------------------------------------------------------------

    SUBROUTINE dealloc_local_ham_matrix

        INTEGER :: err

        DEALLOCATE (absorb, absorb_sq, &
                    RR, Rinverse, &
                    Rinverse2, &
                    r_greater, r_less_over_r_greater, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error in init_local_ham_matrix')

    END SUBROUTINE dealloc_local_ham_matrix

!-------------------------------------------------------------------------------------

    SUBROUTINE test_derivatives_of_r(var, delta_r_value)
 
        REAL(wp), INTENT(IN) :: var(x_1st:x_last)
        REAL(wp), INTENT(IN) :: delta_r_value
        REAL(wp)             :: r, fd_spacing, inv_fd_spacing, inv_fd_spacing_sq
        COMPLEX(wp)          :: poly(x_1st:x_last)
        COMPLEX(wp)          :: exact_1st_deriv_poly(x_1st:x_last)
        COMPLEX(wp)          :: fd_1st_deriv_poly(x_1st:x_last)
        COMPLEX(wp)          :: fd_2nd_deriv_poly(x_1st:x_last)
        INTEGER              :: i
        COMPLEX(wp)          :: first_2_points(x_1st - 2:x_1st - 1)
        COMPLEX(wp)          :: last_2_points(x_last + 1:x_last + 2)
        COMPLEX(wp)          :: answer1(x_1st:x_last)
        REAL(wp)             :: max_error1, error1

        fd_spacing = delta_r_value
        inv_fd_spacing = 1.0_wp/fd_spacing
        inv_fd_spacing_sq = inv_fd_spacing**2

        ! TEST Get4_1st_Deriv

        DO i = x_1st, x_last
            r = var(i)
            poly(i) = EXP(-r)
            exact_1st_deriv_poly(i) = -poly(i)
        END DO

        ! Need to pass values at 2 points inside poly
        ! Values could be arbitrarily as test of values is only for x_1st+2:x_last-2
        DO i = x_1st - 2, x_1st - 1
            r = var(i + 2) - (2*delta_r_value)
            first_2_points(i) = EXP(-r)
        END DO

        DO i = x_last + 1, x_last + 2
            r = var(i - 2) + (2*delta_r_value)
            last_2_points(i) = EXP(-r)
        END DO

        CALL get4_1st_deriv(poly, fd_1st_deriv_poly, first_2_points, last_2_points, inv_fd_spacing)

        max_error1 = 0.0_wp

        DO i = x_1st + 2, x_last - 2
!           PRINT*,'i=',i,' poly =',poly(i), ' FD 1st deriv=', fd_1st_deriv_poly(i)
            answer1(i) = (poly(i) + fd_1st_deriv_poly(i))
            error1 = ABS(REAL(answer1(i)))
            IF (error1 > max_error1) THEN
                max_error1 = error1
            END IF
        END DO

!       PRINT*,'max error=',max_error1
        CALL assert(max_error1 .le. 0.001, 'FAILURE 1st Deriv in R!')

        ! TEST INCR4_with_2nd_Deriv

        DO i = x_1st, x_last
            r = var(i)
            poly(i) = EXP(-r)
            exact_1st_deriv_poly(i) = poly(i)
        END DO

        ! Need to pass values at 2 points inside poly
        ! Values could be arbitrarily as test of values is only for x_1st+2:x_last-2
        DO i = x_1st - 2, x_1st - 1
            r = var(i + 2) - (2*delta_r_value)
            first_2_points(i) = EXP(-r)
        END DO

        DO i = x_last + 1, x_last + 2
            r = var(i - 2) + (2*delta_r_value)
            last_2_points(i) = EXP(-r)
        END DO

        fd_2nd_deriv_poly = (0.0_wp, 0.0_wp)

        CALL incr4_with_2nd_deriv(poly, fd_2nd_deriv_poly, & 
                                  inv_fd_spacing_sq, first_2_points, last_2_points)

        max_error1 = 0.0_wp

        DO i = x_1st + 2, x_last - 2
            answer1(i) = (poly(i) - fd_2nd_deriv_poly(i))
            error1 = ABS(REAL(answer1(i)))
            IF (error1 > max_error1) THEN
                max_error1 = error1
            END IF
        END DO

        CALL assert(max_error1 .LE. 0.001, 'FAILURE 2nd Deriv in R!')

    END SUBROUTINE test_derivatives_of_r

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv(psi, deriv_psi, R_bndry_lo, R_bndry_hi, inv_fd_spacing)
 
        COMPLEX(wp), INTENT(IN)  :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_hi(x_last + 1:x_last + 2)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        INTEGER                  :: i
        REAL(wp)                 :: D1(-2:2)

        ! h_psi_b(1:2) are values at x_1st - 1, x_1st - 2

        !------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        deriv_psi(x_last) = &
            D1(-2)*psi(x_last - 2) + &
            D1(-1)*psi(x_last - 1) + &
            D1(1)*R_bndry_hi(x_last + 1) + &
            D1(2)*R_bndry_hi(x_last + 2)

        deriv_psi(x_last - 1) = &
            D1(-2)*(psi(x_last - 3) - R_bndry_hi(x_last + 1)) + &
            D1(-1)*(psi(x_last - 2) - psi(x_last))

        DO i = x_1st + 2, x_last - 2

            deriv_psi(i) = &
                D1(1)*(psi(i + 1) - psi(i - 1)) + &
                D1(2)*(psi(i + 2) - psi(i - 2))

        END DO

        deriv_psi(x_1st + 1) = &
            D1(1)*(psi(x_1st + 2) - psi(x_1st)) + &
            D1(2)*(psi(x_1st + 3) - R_bndry_lo(x_1st - 1))

        deriv_psi(x_1st) = &
            D1(1)*(psi(x_1st + 1) - R_bndry_lo(x_1st - 1)) + &
            D1(2)*(psi(x_1st + 2) - R_bndry_lo(x_1st - 2))

    END SUBROUTINE get4_1st_deriv

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv_at_b(psi_near_b, deriv_psi_at_b, &
                                   r_bndry_lo, inv_fd_spacing)
 
        COMPLEX(wp), INTENT(IN)  :: psi_near_b(x_1st+1:x_1st + 2)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi_at_b
        COMPLEX(wp), INTENT(IN)  :: r_bndry_lo(x_1st - 2:x_1st - 1)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        REAL(wp)                 :: D1(-2:2)

        !------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        deriv_psi_at_b = &
            D1(1)*(psi_near_b(x_1st + 1) - r_bndry_lo(x_1st - 1)) + &
            D1(2)*(psi_near_b(x_1st + 2) - r_bndry_lo(x_1st - 2))

        ! If an offset outer grid is used, calculate the first derivative coefficients
        IF (offset > 0.0_wp) THEN

            D1(-half_fd_order:half_fd_order) = quad_and_fd_coeffs(1:2*half_fd_order + 1, 2)
            D1 = D1*inv_fd_spacing

            deriv_psi_at_b = &
                D1(-2)*r_bndry_lo(x_1st - 2) + &
                D1(-1)*r_bndry_lo(x_1st - 1) + &
                D1(0)*psi_near_b(x_1st) + &
                D1(1)*psi_near_b(x_1st + 1) + &
                D1(2)*psi_near_b(x_1st + 2)
        END IF

    END SUBROUTINE get4_1st_deriv_at_b

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv_b(psi, deriv_psi, h_psi_b, &
                                inv_fd_spacing, num_grid_points)
 
        INTEGER, INTENT(IN)      :: num_grid_points
        COMPLEX(wp), INTENT(IN)  :: psi(1:num_grid_points)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(IN)  :: h_psi_b(1:4)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        INTEGER                  :: i
        REAL(wp)                 :: D1(-2:2)

        ! h_psi_b(1:4) are values at first_pt - 2, first_pt - 1, last_pt + 1, last_pt + 2
 
        !-------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !-------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        IF (num_grid_points .EQ. 1) THEN

            deriv_psi(1) = D1(-2)*(h_psi_b(1) - h_psi_b(4)) + &
                           D1(-1)*(h_psi_b(2) - h_psi_b(3))

        ELSE IF (num_grid_points .EQ. 2) THEN

            deriv_psi(2) = D1(-2)*(h_psi_b(2) - h_psi_b(4)) + &
                           D1(-1)*(psi(1) - h_psi_b(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - h_psi_b(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE IF (num_grid_points .EQ. 3) THEN

            deriv_psi(3) = D1(-2)*(psi(1) - h_psi_b(4)) + &
                           D1(-1)*(psi(2) - h_psi_b(3))
            deriv_psi(2) = D1(-2)*(h_psi_b(2) - h_psi_b(3)) + &
                           D1(-1)*(psi(1) - psi(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - psi(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE IF (num_grid_points .EQ. 4) THEN

            deriv_psi(4) = D1(-2)*(psi(2) - h_psi_b(4)) + &
                           D1(-1)*(psi(3) - h_psi_b(3))
            deriv_psi(3) = D1(-2)*(psi(1) - h_psi_b(3)) + &
                           D1(-1)*(psi(2) - psi(4))
            deriv_psi(2) = D1(-2)*(h_psi_b(2) - psi(4)) + &
                           D1(-1)*(psi(1) - psi(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - psi(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE

            deriv_psi(num_grid_points) = &
                D1(-2)*(psi(num_grid_points - 2) - h_psi_b(4)) + &
                D1(-1)*(psi(num_grid_points - 1) - h_psi_b(3))

            deriv_psi(num_grid_points - 1) = &
                D1(-2)*(psi(num_grid_points - 3) - h_psi_b(3)) + &
                D1(-1)*(psi(num_grid_points - 2) - psi(num_grid_points))

            DO i = 3, num_grid_points - 2

                deriv_psi(i) = &
                    D1(1)*(psi(i + 1) - psi(i - 1)) + &
                    D1(2)*(psi(i + 2) - psi(i - 2))
            END DO

            deriv_psi(2) = &
                D1(1)*(psi(3) - psi(1)) + &
                D1(2)*(psi(4) - h_psi_b(2))

            deriv_psi(1) = &
                D1(1)*(psi(2) - h_psi_b(2)) + &
                D1(2)*(psi(3) - h_psi_b(1))

        END IF

    END SUBROUTINE get4_1st_deriv_b

!---------------------------------------------------------------------

    SUBROUTINE incr_with_laplacian(my_psi, RESULT, &
                                   L, second_deriv_coeff, atomic_ham_factor, &
                                   one_over_r_sq, R_bndry_lo, R_bndry_hi)

        COMPLEX(wp), INTENT(IN)    :: my_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(INOUT) :: RESULT(x_1st:x_last)

        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: one_over_r_sq(x_1st:x_last)
        INTEGER, INTENT(IN)        :: L
        COMPLEX(wp), INTENT(IN)    :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_hi(x_last + 1:x_last + 2)

        ! Increment Result with the Centripetal terms from the Kinetic Ham.

        CALL apply_centripetal(my_psi, RESULT, L,atomic_ham_factor,one_over_r_sq, RR(x_1st) - offset*deltaR, x_last - x_1st + 1)

        CALL incr4_with_2nd_deriv(my_psi, RESULT, second_deriv_coeff, R_bndry_lo, R_bndry_hi)

    END SUBROUTINE incr_with_laplacian

    SUBROUTINE apply_centripetal (psi, h_psi, L, atomic_ham_factor, one_over_r_sq, r_val_at_bndry, number_grid_points)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: h_psi(number_grid_points)
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: r_val_at_bndry
        REAL(wp), INTENT(IN)       :: one_over_r_sq(number_grid_points)
        INTEGER, INTENT(IN)        :: L
        REAL(wp)                   :: centripetal(number_grid_points)
        INTEGER                    :: i, limit
        REAL(wp)                   :: C1, CL, delta_centrip_at_limit

        centripetal = REAL(L*(L + 1), wp)*atomic_ham_factor*one_over_r_sq

        IF ((r_val_at_bndry .LT. R_cutoff) .AND. (L .GT. Lmax_At_bndry)) THEN

            limit = NINT((R_cutoff - r_val_at_bndry)/deltaR)
            CL = REAL(Lmax_At_bndry*(Lmax_At_bndry + 1), wp)

            delta_centrip_at_limit = centripetal(limit) - CL*atomic_ham_factor*one_over_r_sq(limit)

            DO i = 1, limit - 1
                C1 = CL*atomic_ham_factor*one_over_r_sq(i)
                centripetal(i) = C1 + delta_centrip_at_limit
            END DO

        END IF

        DO i = 1, number_grid_points
            h_psi(i) = h_psi(i) - psi(i)*centripetal(i)
        END DO

    
    END SUBROUTINE apply_centripetal
!---------------------------------------------------------------------

    SUBROUTINE incr_with_laplacian_b(my_psi, RESULT, &
                                     L, second_deriv_coeff, atomic_ham_factor, &
                                     one_over_r_sq, h_psi_b, num_grid_points, &
                                     r_value_inner_bndry)

        INTEGER, INTENT(IN)        :: num_grid_points
        COMPLEX(wp), INTENT(IN)    :: my_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: RESULT(1:num_grid_points)

        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: one_over_r_sq(1:num_grid_points)
        INTEGER, INTENT(IN)        :: L
        REAL(wp), INTENT(IN)       :: r_value_inner_bndry
        COMPLEX(wp), INTENT(IN)    :: h_psi_b(1:4)

        ! Increment Result with the Centripetal terms from the Kinetic Ham.

        CALL apply_centripetal(my_psi, RESULT, L,atomic_ham_factor,one_over_r_sq, r_value_inner_bndry, num_grid_points)

        CALL incr4_with_2nd_deriv_b(my_psi, RESULT, second_deriv_coeff, h_psi_b, num_grid_points)

    END SUBROUTINE incr_with_laplacian_b

!--------------------------------------------------------------------------------------

    SUBROUTINE incr4_with_2nd_deriv(psi, deriv_psi, second_deriv_coeff, R_bndry_lo, R_bndry_hi)

        COMPLEX(wp), INTENT(IN)    :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(INOUT) :: deriv_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_hi(x_last + 1:x_last + 2)
        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        INTEGER                    :: i
        REAL(wp)                   :: D2(-2:2)

        ! 3rd ORDER DIFFERENTIATION CONSTANTS (2ND DERIVATIVE)

!       D2(2)  =  -1.0_wp  / 12.0_wp
!       D2(1)  =   16.0_wp / 12.0_wp
!       D2(0)  =  -30.0_wp / 12.0_wp
!       D2(-1) =   16.0_wp / 12.0_wp
!       D2(-2) =  -1.0_wp  / 12.0_wp

        D2(2) = -0.083333333333333333333333333_wp
        D2(1) = 1.333333333333333333333333333_wp
        D2(0) = -2.500000000000000000000000000_wp
        D2(-1) = 1.333333333333333333333333333_wp
        D2(-2) = -0.083333333333333333333333333_wp

        DO i = -2, 2
            D2(i) = D2(i)*second_deriv_coeff
        END DO

        deriv_psi(x_last) = deriv_psi(x_last) + &
                            D2(-2)*psi(x_last - 2) + &
                            D2(-1)*psi(x_last - 1) + &
                            D2(0)*psi(x_last) + &
                            D2(1)*R_bndry_hi(x_last + 1) + &
                            D2(2)*R_bndry_hi(x_last + 2)

        deriv_psi(x_last - 1) = deriv_psi(x_last - 1) + &
                                D2(-2)*psi(x_last - 3) + &
                                D2(-1)*psi(x_last - 2) + &
                                D2(0)*psi(x_last - 1) + &
                                D2(1)*psi(x_last) + &
                                D2(2)*R_bndry_hi(x_last + 1)

        DO i = x_1st + 2, x_last - 2

            deriv_psi(i) = deriv_psi(i) + &
                           D2(-2)*psi(i - 2) + &
                           D2(-1)*psi(i - 1) + &
                           D2(0)*psi(i) + &
                           D2(1)*psi(i + 1) + &
                           D2(2)*psi(i + 2)
        END DO

        deriv_psi(x_1st + 1) = deriv_psi(x_1st + 1) + &
                               D2(-2)*R_bndry_lo(x_1st - 1) + &
                               D2(-1)*psi(x_1st) + &
                               D2(0)*psi(x_1st + 1) + &
                               D2(1)*psi(x_1st + 2) + &
                               D2(2)*psi(x_1st + 3)

        deriv_psi(x_1st) = deriv_psi(x_1st) + &
                           D2(-2)*R_bndry_lo(x_1st - 2) + &
                           D2(-1)*R_bndry_lo(x_1st - 1) + &
                           D2(0)*psi(x_1st) + &
                           D2(1)*psi(x_1st + 1) + &
                           D2(2)*psi(x_1st + 2)

    END SUBROUTINE incr4_with_2nd_deriv

!--------------------------------------------------------------------

    SUBROUTINE incr4_with_2nd_deriv_b(psi, deriv_psi, second_deriv_coeff, h_psi_b, &
                                      num_grid_points)

        INTEGER, INTENT(IN)        :: num_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(1:num_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: deriv_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(IN)    :: h_psi_b(1:4)
        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        INTEGER                    :: i
        REAL(wp)                   :: D2(-2:2)

        ! h_psi_b(1:4) are values at first_pt - 2, first_pt - 1, last_pt + 1, last_pt + 2

        ! 3rd ORDER DIFFERENTIATION CONSTANTS (2ND DERIVATIVE)

!       D2(2)  =  -1.0_wp  / 12.0_wp
!       D2(1)  =   16.0_wp / 12.0_wp
!       D2(0)  =  -30.0_wp / 12.0_wp
!       D2(-1) =   16.0_wp / 12.0_wp
!       D2(-2) =  -1.0_wp  / 12.0_wp

        D2(2) = -0.083333333333333333333333333_wp
        D2(1) = 1.333333333333333333333333333_wp
        D2(0) = -2.500000000000000000000000000_wp
        D2(-1) = 1.333333333333333333333333333_wp
        D2(-2) = -0.083333333333333333333333333_wp

        DO i = -2, 2
            D2(i) = D2(i)*second_deriv_coeff
        END DO

        IF (num_grid_points .EQ. 1) THEN

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

        ELSE IF (num_grid_points .EQ. 2) THEN

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*h_psi_b(3)

        ELSE IF (num_grid_points .EQ. 3) THEN

            deriv_psi(3) = deriv_psi(3) + &
                           D2(-2)*psi(1) + &
                           D2(-1)*psi(2) + &
                           D2(0)*psi(3) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*h_psi_b(3)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        ELSE IF (num_grid_points .EQ. 4) THEN

            deriv_psi(4) = deriv_psi(4) + &
                           D2(-2)*psi(2) + &
                           D2(-1)*psi(3) + &
                           D2(0)*psi(4) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(3) = deriv_psi(3) + &
                           D2(-2)*psi(1) + &
                           D2(-1)*psi(2) + &
                           D2(0)*psi(3) + &
                           D2(1)*psi(4) + &
                           D2(2)*h_psi_b(3)

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*psi(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        ELSE

            deriv_psi(num_grid_points) = deriv_psi(num_grid_points) + &
                                         D2(-2)*psi(num_grid_points - 2) + &
                                         D2(-1)*psi(num_grid_points - 1) + &
                                         D2(0)*psi(num_grid_points) + &
                                         D2(1)*h_psi_b(3) + &
                                         D2(2)*h_psi_b(4)

            deriv_psi(num_grid_points - 1) = deriv_psi(num_grid_points - 1) + &
                                             D2(-2)*psi(num_grid_points - 3) + &
                                             D2(-1)*psi(num_grid_points - 2) + &
                                             D2(0)*psi(num_grid_points - 1) + &
                                             D2(1)*psi(num_grid_points) + &
                                             D2(2)*h_psi_b(3)

            DO i = 3, num_grid_points - 2

                deriv_psi(i) = deriv_psi(i) + &
                               D2(-2)*psi(i - 2) + &
                               D2(-1)*psi(i - 1) + &
                               D2(0)*psi(i) + &
                               D2(1)*psi(i + 1) + &
                               D2(2)*psi(i + 2)
            END DO

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*psi(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        END IF

    END SUBROUTINE incr4_with_2nd_deriv_b

!---------------------------------------------------------------------
! Akin to absorbing bc.  Split Psi into 2 parts.  Throw one away.
!---------------------------------------------------------------------

    SUBROUTINE local_split(del_R, psi, discarded_population, &
                           discarded_population_si, single_ioniz_bndry)

        REAL(wp), INTENT(IN)       :: del_R
        COMPLEX(wp), INTENT(INOUT) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)      :: discarded_population
        REAL(wp), INTENT(OUT)      :: discarded_population_si
        INTEGER, INTENT(IN)        :: single_ioniz_bndry

        INTEGER  :: i
        REAL(wp) :: sum_discarded_pop, sum_discarded_pop2, mod_1_minus_A


        ! Keep track of discarded population:
        sum_discarded_pop = 0.0_wp
        sum_discarded_pop2 = 0.0_wp
!       mod_1_minus_A = 1.0_ep - (Absorb)**2

        DO i = x_1st, x_last

            mod_1_minus_A = 1.0_wp - Absorb_sq(i)
            sum_discarded_pop = sum_discarded_pop + &
                                mod_1_minus_A*REAL(CONJG(psi(i))*psi(i), wp)

            ! Keep track of discarded population in the single ionization region:
            IF (R_id_at(i) > single_ioniz_bndry) THEN
                sum_discarded_pop2 = sum_discarded_pop2 + &
                                     mod_1_minus_A*REAL(CONJG(psi(i))*psi(i), wp)
            END IF

        END DO

        discarded_population = sum_discarded_pop*del_R
        discarded_population_si = sum_discarded_pop2*del_R

        ! Do absorption on psi:
        DO i = x_1st, x_last
            psi(i) = Absorb(i)*psi(i)
        END DO

    END SUBROUTINE local_split

!---------------------------------------------------------------------
!Absorption pot. (multiply this by the wave function)
!---------------------------------------------------------------------

    SUBROUTINE init_absorb

        USE initial_conditions, ONLY: start_factor, &
                                      sigma_factor

        INTEGER     :: i

        REAL(wp)    :: halfbox, sigma
        INTEGER     :: R_absorb_start


        absorb = 1.0_wp
        absorb_sq = 1.0_wp

        R_absorb_start = NINT(REAL(R_last, wp)*start_factor)

        halfbox = (R_last/2)*deltaR

        sigma = halfbox*sigma_factor

        DO i = x_1st, x_last
            IF ((R_id_at(i) - R_absorb_start) > 0) THEN
                absorb(i) = EXP(-(REAL(R_id_at(i) - R_absorb_start, wp)*deltaR/sigma)**2)
                absorb_sq(i) = absorb(i)**2
            END IF
        END DO

    END SUBROUTINE init_absorb

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_A(del_R, psi, population)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Total outer region

        ! Trapezoidal rule (with weighting of 1/2 for 1st grid point):

        sum_population = 0.0_wp
        DO i1 = x_1st + 1, x_last
            sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
        END DO

        IF (R_id_at(x_1st) .EQ. 1) THEN
            sum_population = sum_population + 0.50_wp*REAL(CONJG(psi(x_1st))*psi(x_1st), wp)
        ELSE
            sum_population = sum_population + REAL(CONJG(psi(x_1st))*psi(x_1st), wp)
        END IF

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_A

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer(del_R, psi, population)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Total outer region

        sum_population = 0.0_wp

        ! Hugo: with rweights defined properly, this should not be a problem

        DO i1 = x_1st, x_last
            sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp) &
                             *rweight(i1)
        END DO

        ! Simpson's rule:
        ! Weights applied to grid point 1,2,3,4,5,6,7,....R_Last are 1,4,2,4,2,4,2,...1

        ! Hugo: this all commented out
!       IF (MOD(R_id_at(x_1st),2) .EQ. 0) THEN ! x_1st is an even grid point
!           DO i1 = x_1st+1,x_last-1,2
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
!           DO i1 = x_1st,x_last-1,2 ! Include x_1st in this loop (x_1st is even so cannot equal 1)
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
            ! Add on contribution from x_last:
!           IF (R_id_at(x_last) .EQ. (R_Last-R_1st+1)) THEN ! x_last grid point = R_Last
!               sum_population = sum_population + REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE IF (MOD(R_id_at(x_last),2) .EQ. 0)  THEN ! x_last is an even grid point
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE  ! x_last is an odd grid point
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           END IF
!       ELSE  ! x_1st is an odd grid point
!           DO i1 = x_1st+1,x_last-1,2
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
!           DO i1 = x_1st+2,x_last-1,2
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
            ! Add on contribution from x_1st:
!           IF (R_id_at(x_1st) .EQ. 1) THEN ! first grid point on the boundary with the inner region
!               sum_population = sum_population + REAL (CONJG (psi(x_1st))*psi(x_1st), wp)
!           ELSE
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_1st))*psi(x_1st), wp)
!           END IF
            ! Add on contribution from x_last:
!           IF (R_id_at(x_last) .EQ. (R_Last-R_1st+1)) THEN ! x_last grid point = R_Last
!               sum_population = sum_population + REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE IF (MOD(R_id_at(x_last),2) .EQ. 0)  THEN ! x_last is an even grid point
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE  ! x_last is an odd grid point
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           END IF
!       END IF

        population = sum_population

!       population = population*(del_R/3.0_wp)
        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_si(del_R, psi, population, single_ioniz_bndry)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population
        INTEGER, INTENT(IN)     :: single_ioniz_bndry

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Outer region beyond certain distance - SI (Single Ionization)
        ! Using trapezoidal rule

        sum_population = 0.0_wp
        DO i1 = x_1st, x_last
            IF (R_id_at(i1) > single_ioniz_bndry) THEN
                sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
            END IF
        END DO

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_si

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_ryd(del_R, psi, population, single_ioniz_bndry)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population
        INTEGER, INTENT(IN)     :: single_ioniz_bndry

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Outer region beyond certain distance - SI (Single Ionization)
        ! Using trapezoidal rule

        sum_population = 0.0_wp
        DO i1 = x_1st, x_last
            IF (R_id_at(i1) .LE. single_ioniz_bndry) THEN
                sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
            END IF
        END DO

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_ryd

!-----------------------------------------------------------------------

!> \brief Calculate quadrature weights and finite-difference coefficients for the
!>        first derivative at the boundary for a general offset of the outer region
!>        radial grid.
!>
!>        Quadrature coefficients are calculated by integrating polynomials of order
!>        zero to four over an interval of five points, starting at offset*deltaR and
!>        ending at (4.0 + offset)*deltaR.
!>
!>  \param[in]    offset              The offset of the outer-region grid points, in units of deltaR
!>  \param[out]   quad_and_fd_coeffs  Array containing the calculated quadrature and finite-difference
!>                                    coefficients for the first derivative at b.
!>
    SUBROUTINE calculate_quadrature_and_1st_deriv_coeffs(offset, quad_and_fd_coeffs)

       USE calculation_parameters, ONLY: half_fd_order

       REAL(wp), INTENT(IN)  :: offset
       REAL(wp), INTENT(OUT) :: quad_and_fd_coeffs(2*half_fd_order + 1, 2)

       INTEGER, PARAMETER    :: order = 2 * half_fd_order + 1
       INTEGER               :: i, j
       INTEGER               :: LDA, LDB
       INTEGER               :: ipiv(order)
       INTEGER               :: info
       INTEGER               :: deriv_order_reqd
       REAL(wp)              :: A(order, order)
       REAL(wp)              :: b(order)
       REAL(wp)              :: start

!>     Calculate quadrature weights for a general outer-region offset.
!>     Integrate x^(n-1) for n = 1, 5 from 0 to (4.0 + offset)*deltaR
!>     using function evaluations at five grid points (offset*deltaR up to (4 + offset)*deltaR)
!>     and solve for the coefficients. For each n this gives an equation
!>     \f [
!>         a(o)^{n-1} + b(1 + o)^{n-1} + c(2 + o)^{n-1} + d(3 + o)^{n-1} + e(4 + o)^{n-1} = (4 + o)^n /n
!>     \f]
!>     where o is the offset, and (abcde) are the coefficients to be found.
!>     A factor of deltaR is assumed within each of the coefficients, so that
!>     factors of (deltaR)^n on both sides of the equation can be cancelled.
!>     (This extra factor of deltaR is applied in global_linear_algebra, in
!>     the local_inner_product function for example.)
!>     This gives 5 simultaneous equations for n = 1,5 that are solved using DGESV.
!>
!>     Matrix A evaluates the integrand x^(j-1) at the grid points for i = 1, order
!>     RHS b is the result of the integral
       A = 0.0_wp; b = 0.0_wp
       DO i = 1, order
          DO j = 1, order
             A(j, i) = (REAL(i-1, wp) + offset)**(j - 1)
          END DO
          b(i) = (4.0_wp + offset)**(i) / REAL(i, wp)
       END DO

       LDA = order; LDB = order
       CALL DGESV(order, 1, A, LDA, ipiv, b, LDB, info)
!>     Store quadrature weights in first column of quad_and_fd_coeffs
       quad_and_fd_coeffs(:, 1) = b(:)

!>     Calculate first derivative finite-difference coefficients for a general outer-region offset.
!>     The first derivative is calculated as
!>     \f [
!>         \frac{df}{dr} = \frac{1}{\delta r}
!>                         \left[ a f(r - (2 - o)\delta r + bf(r - (1 - o)\delta r)
!>                              + c f(r + o\delta r) + d f(r + (1 + o)\delta r)
!>                              + e f(r + (2 + o)\delta r)
!>     ] \f,
!>     where $o$ is the offset_in_stepsizes parameter.
!>     Expansion of each of the function evaluations around r gives
!>     \f [
!>     \frac{df}{dr} = \sum_{n=0}^{\infty} \left\lbrace
!>                          \frac{1}{n!}\left[
!>                          a(o - 2)^n + b(o-1)^n + co^n + d(o + 1)^n + e(o + 2)^n
!>                          \right]\delta r^{n-1} \frac{d^n f}{dr^n}
!>                                          \right\rbrace
!>     ] \f
!>
!>     Solving for the coefficients for n = 0 to n = 4, with each row of the matrix
!>     corresponding to a particular n, and with column i having the value (-2 + o + (i-1))^{row - 1},
!>     The right hand side column vector has entries of zero apart from the second entry,
!>     which is the coefficient of df/dr, i.e. 1.
!>
       A = 0.0_wp; b = 0.0_wp
       start = -2.0_wp + offset
       DO i = 1, order
          DO j = 1, order
             A(j, i) = (REAL(i - 1, wp) + start)**(j - 1)
          END DO
       END DO
!>     Coefficients required for first derivative only,
!>     so only the second element of c should be non-zero,
!>     and all other elements are zero.
       deriv_order_reqd = 1 
       b(deriv_order_reqd + 1) = 1.0_wp

       LDA = order; LDB = order
       CALL DGESV(order, 1, A, LDA, ipiv, b, LDB, info)
!>     Store finite-difference coefficients for first derivative at b in second column of quad_and_fd_coeffs
       quad_and_fd_coeffs(:, 2) = b(:)

    END SUBROUTINE calculate_quadrature_and_1st_deriv_coeffs

END MODULE local_ham_matrix
