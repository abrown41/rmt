from os import path
from setuptools import setup, find_packages
import sys
import versioneer


# NOTE: This file must remain Python 2 compatible for the foreseeable future,
# to ensure that we error out properly for people with outdated setuptools
# and/or pip.
min_version = (3, 8)
if sys.version_info < min_version:
    error = """
rmt-utilities does not support Python {0}.{1}.
Python {2}.{3} and above is required. Check your Python version like so:

python3 --version

This may be due to an out-of-date pip. Make sure you have pip >= 9.0.1.
Upgrade pip like so:

pip install --upgrade pip
""".format(*(sys.version_info[:2] + min_version))
    sys.exit(error)

here = path.abspath(path.dirname(__file__))

with open(path.join(here, './docs/source/README.rst'), encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(path.join(here, 'requirements.txt')) as requirements_file:
    # Parse requirements.txt, ignoring any commented-out lines.
    requirements = [line for line in requirements_file.read().splitlines()
                    if not line.startswith('#')]

setup(
    name='rmt-utilities',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Python package for interacting with the high performance,\
                 parallel fortran code R-matrix with Time",
    long_description=readme,
    author="Andrew Brown",
    author_email='andrew.brown@qub.ac.uk',
    url='https://gitlab.com/abrown41/rmt-utilities',
    python_requires='>={}'.format('.'.join(str(n) for n in min_version)),
    packages=find_packages(where='source', exclude=['docs', 'tests']),
    entry_points={
        'console_scripts': [
            'RMT_analyse_hd = rmt_utilities.analyse_hd:_main',
            'RMT_cross_sec = rmt_utilities.cross_sec:main',
            'RMT_gen_hhg = rmt_utilities.gen_hhg:main',
            'RMT_gen_tas = rmt_utilities.gen_tas:main',
            'RMT_compare = rmt_utilities.rmt_compare:main',
            'RMT_plot_mom = rmt_utilities.plot_dist:plot_mom',
            'RMT_plot_den = rmt_utilities.plot_dist:plot_den',
            'RMT_data_recon = rmt_utilities.data_recon:recon',
            'RMT_OR_size = rmt_utilities.OR_size:main',
            'RMT_fetch_data = rmt_utilities.fetchtests:main'
        ],
    },
    include_package_data=True,
    package_dir={'': 'source'},
    install_requires=requirements,
    license="GPL-v3",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.8',
    ],
)
