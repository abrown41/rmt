gen_hhg command
***********************

.. automodule:: rmt_utilities.gen_hhg
.. argparse::
   :module: rmt_utilities.dipole_cli
   :func: read_command_line
   :prog: RMT_gen_hhg

