from rmt_utilities import regress as rg
from shutil import which
from os import getenv
import pytest

mpiRun = which('mpirun')
rmtexec = getenv('RMT_EXEC')
inpfile = 'tests/rapidtests.yml'
mpiopts = "--allow-run-as-root --oversubscribe"

tests = rg.RMT_regression_tests(inpfile, rmtexec=rmtexec,
                                mpiRun=mpiRun, mpiopts=mpiopts)


@pytest.mark.parametrize("testcalc", tests)
def test_RMTcalc(testcalc):
    assert testcalc.runTest()
    testcalc.cleanupdir()
